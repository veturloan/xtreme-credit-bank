<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <script>window.azameoSite = "onyxunionbank";</script>
    <script type="text/javascript" src="//tag.azame.net/tag/script.js" async = "true"></script>
    <noscript>
    <link href="https://tag.azame.net/tag/style.css" rel="stylesheet" media="all" type="text/css">
    </noscript>
    <!-- Title -->
    <title>{{ config('app.name') }} | @yield('title',__('a credit that makes you enjoy'))</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('favicon.png') }}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:400,500,600" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ url('css/all.css') }}">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="{{ asset('assets2/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" href="{{ asset('assets2/vendor/revolution-slider/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets2/vendor/revolution-slider/revolution/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets2/vendor/revolution-slider/revolution/css/navigation.css') }}">

    <style>

        body {
            background-color: #ffffff!important;
        }

        .no-border-radius {
            border-radius: unset!important;
        }

        .no-border {
            border: unset!important;
        }

        .account-btn {
            background-color: #FB8500;border:unset!important;
        }
        .account-btn:hover {
            background-color: #ffffff;color:#FB8500!important;
        }
        .login-btn {
            background-color: #ffffff;border: unset!important;color: #FB8500;border-radius: unset!important;
        }
        .login-btn:hover {
            background-color: #FB8500;color:#ffffff!important;
        }
        .g-color-red {
            color:#219EBC!important;
        }
        .u-btn-red, .g-bg-red {
            background-color: #219EBC!important;color:#ffffff!important;
        }

        .u-header__section--light .navbar-nav:not([class*=u-main-nav-v]) .nav-link, .u-header__section--light--shift.js-header-change-moment .navbar-nav:not([class*=u-main-nav-v]) .nav-link {
            color: #FB8500!important;
        }

        .g-bg-blue {
            background-color: #FB8500!important;
        }

        .u-header__section--light .navbar-nav:not([class*=u-main-nav-v]) .nav-link, .u-header__section--light--shift.js-header-change-moment .navbar-nav:not([class*=u-main-nav-v]) .nav-link {
            color: #ffffff!important;
        }


        element.style {
            display: block;
        }
        .u-header [aria-labelledby] {
            opacity: 0;
        }
        .u-header [aria-labelledby] {
            opacity: 0;
        }
        .hs-mega-menu-opened>.hs-mega-menu, .hs-sub-menu-opened>.hs-sub-menu {
            visibility: visible;
            opacity: 1;
        }
        .hs-menu-initialized .animated {
            -webkit-animation-duration: .3s;
            animation-duration: .3s;
        }
        @media (min-width: 0)
            .g-mt-7 {
                margin-top: .5rem!important;
            }
            .g-min-width-220 {
                min-width: 220px;
            }
            .g-brd-primary {
                border-color: #efb225!important;
            }
            .g-brd-top-2 {
                border-top-width: 2px!important;
            }
            .g-brd-top {
                border-top: solid 1px transparent!important;
            }
            .u-shadow-v11 {
                box-shadow: 0 0 2px #ccc;
            }
            @media (min-width: 0)
                .g-mt-7 {
                    margin-top: .5rem!important;
                }
                .g-min-width-220 {
                    min-width: 220px;
                }
                .g-brd-primary {
                    border-color: #72c02c!important;
                }
                .g-brd-top-2 {
                    border-top-width: 2px!important;
                }
                .g-brd-top {
                    border-top: solid 1px transparent!important;
                }
                .u-shadow-v11 {
                    -webkit-box-shadow: 0 0 2px #ccc;
                    box-shadow: 0 0 2px #ccc;
                }
                .hs-sub-menu {
                    min-width: 180px;
                }
                .hs-mega-menu, .hs-sub-menu {
                    background-color: #0b011d;
                }


    </style>

</head>

<body>
<main>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--static u-shadow-v19">
        @include('includes.top-bar-2')

        @include('includes.menu-2')
    </header>
    <!-- End Header -->

    @yield('content')

    @include('includes.footer-2')

</main>
<script src="{{ url('js/all.js') }}"></script>
<!-- JS Revolution Slider -->
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets2/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

<script>
    // Revolution Slider
    var tpj = jQuery;

    var revapi1086;
    tpj(document).ready(function () {
        if (tpj("#rev_slider_1086_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1086_1");
        } else {
            revapi1086 = tpj("#rev_slider_1086_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "revolution/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    mouseScrollReverse: "default",
                    onHoverStop: "on",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 50,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '',
                        left: {
                            h_align: "right",
                            v_align: "bottom",
                            h_offset: 40,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 0
                        }
                    }
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1200, 1024, 778, 480],
                gridheight: [600, 600, 600, 600],
                lazyType: "single",
                parallax: {
                    type: "scroll",
                    origo: "slidercenter",
                    speed: 400,
                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
                    type: "scroll",
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    });
</script>

@yield('scripts')
</body>
</html>

