const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.styles(
    [
        'public/assets2/vendor/bootstrap/bootstrap.min.css',
        'public/assets2/vendor/bootstrap/offcanvas.css',
        'public/assets2/vendor/icon-awesome/css/font-awesome.min.css',
        'public/assets2/vendor/icon-line/css/simple-line-icons.css',
        'public/assets2/vendor/icon-etlinefont/style.css',
        'public/assets2/vendor/icon-line-pro/style.css',
        'public/assets2/vendor/icon-hs/style.css',
        'public/assets2/vendor/dzsparallaxer/dzsparallaxer.css',
        'public/assets2/vendor/dzsparallaxer/dzsscroller/scroller.css',
        'public/assets2/vendor/dzsparallaxer/advancedscroller/plugin.css',
        'public/assets2/vendor/animate.css',
        'public/assets2/vendor/hs-megamenu/src/hs.megamenu.css',
        'public/assets2/vendor/hamburgers/hamburgers.min.css',
        'public/assets2/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css',
        'public/assets2/vendor/fancybox/jquery.fancybox.css',
        'public/assets2/vendor/slick-carousel/slick/slick.css',
        'public/assets2/css/unify-core.css',
        'public/assets2/css/unify-components.css',
        'public/assets2/css/unify-globals.css',
        'public/assets2/css/styles.multipage-marketing.css',
        'public/assets2/vendor/master-slider/source/assets/css/masterslider.main.css',
        'public/assets/css/style.css',
        'public/assets2/css/custom.css',

    ],
    'public/css/all.css'
).options({
    processCssUrls: false
});

mix.scripts(
    [
        'public/assets2/vendor/jquery/jquery.min.js',
        'public/assets2/vendor/jquery-migrate/jquery-migrate.min.js',
        'public/assets2/vendor/popper.js/popper.min.js',
        'public/assets2/vendor/bootstrap/bootstrap.min.js',
        'public/assets2/vendor/bootstrap/offcanvas.js',
        'public/assets2/vendor/hs-megamenu/src/hs.megamenu.js',
        'public/assets2/vendor/dzsparallaxer/dzsparallaxer.js',
        'public/assets2/vendor/dzsparallaxer/dzsscroller/scroller.js',
        'public/assets2/vendor/dzsparallaxer/advancedscroller/plugin.js',
        'public/assets2/vendor/masonry/dist/masonry.pkgd.min.js',
        'public/assets2/vendor/imagesloaded/imagesloaded.pkgd.min.js',
        'public/assets2/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js',
        'public/assets2/vendor/slick-carousel/slick/slick.js',
        'public/assets2/vendor/fancybox/jquery.fancybox.min.js',
        'public/assets2/js/hs.core.js',
        'public/assets2/js/components/hs.header.js',
        'public/assets2/js/helpers/hs.hamburgers.js',
        'public/assets2/js/components/hs.dropdown.js',
        'public/assets2/js/components/hs.scrollbar.js',
        'public/assets2/js/components/hs.popup.js',
        'public/assets2/js/components/hs.carousel.js',
        'public/assets2/js/components/hs.go-to.js',
        'public/assets2/js/components/hs.sticky-block.js',
        'public/assets2/vendor/master-slider/source/assets/js/masterslider.min.js',
        'public/assets2/vendor/master-slider/source/assets/js/masterslider.min.js',
        'public/assets2/js/custom.js',
    ],
    'public/js/all.js'
);
