<!-- Promo Block -->
<div class="g-mt-120--lg g-mt-120--md dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
    <div class="divimage dzsparallaxer--target w-100 g-bg-cover g-bg-black-opacity-0_3--after" style="height: 100%; background-image: url({{ url('images/img1.jpg') }});"></div>

    <div class="container " style="padding-top: 200px">
        <div class="row justify-content-between align-items-center">
            <div class="col-md-6 col-lg-7 g-mb-90">
                <!-- Content Info -->
                <div class="mb-5">
                    <h1 class="g-color-white g-font-weight-600 g-font-size-50 mb-3">{{ __('Fast. Flexible. Sure.') }}</h1>
                    <p class="g-color-white g-font-size-18 g-line-height-2">{{ __('XTREME CREDIT BANK offers you all its know-how in the sale of remote credit solutions. Personal loans, revolving loans, credit repurchase, but also insurance products are now available to best meet your needs.') }}</p>
                </div>
                <a class="btn u-btn-black g-brd-red g-brd-red--hover g-bg-black g-bg-red--hover g-font-weight-600 g-font-size-12 text-uppercase g-px-25 g-py-13" href="{{ route('request',app()->getLocale()) }}">
                    {{ __('Get a loan') }}
                    <i class="g-pos-rel g-top-minus-1 ml-2 fa fa-angle-right"></i>
                </a>
                <a class="btn u-btn-primary g-brd-main--hover g-bg-red g-font-weight-600 g-font-size-12 text-uppercase g-px-25 g-py-13 mr-3" href="{{ env('BANK_SOFT_URL') }}">
                    <i class="g-pos-rel g-top-minus-1 mr-2 fa fa-user"></i>
                    {{ __('My Account') }}
                    <i class="g-pos-rel g-top-minus-1 ml-2 fa fa-angle-right"></i>
                </a>
                <!-- End Content Info -->
            </div>

            {{--<div class="col-md-6 col-lg-4 g-mb-90">
                <img src="{{ url('images/yellow-agent.png') }}" class="img-fluid">
                <!-- Join Form -->
                --}}{{--<div class="g-bg-white rounded g-px-30 g-py-40">

                    --}}{{----}}{{--@if(session('result'))
                        <div class="text-center">
                            <h2 class="h4 g-font-weight-600 g-mb-35">{{ __('Simulation results') }}</h2>

                            <h5 class="h5">
                                {{ __('THE AMOUNT OF YOUR MONTHLY PAYMENTS IS') }} <strong class="text-success">{{ session('result')['monthly_payment'] }} {{ session('result')['currency'] }}</strong> {{ __('OF WHICH') }} <strong class="text-success">{{ session('result')['monthly_rate'] }} {{ session('result')['currency'] }}</strong> {{ __('OF RATE') }}
                            </h5>

                            <h5 class="h6">{{ __('Total credit') }} : <strong class="text-primary">{{ session('result')['total_credit'] }} {{ session('result')['currency'] }}</strong></h5>
                            <h5 class="h6">{{ __('Total credit rate') }} : <strong class="text-primary">{{ session('result')['total_rate'] }} {{ session('result')['currency'] }}</strong></h5>

                        </div>
                        <div class="row no-gutters g-mb-10">
                            <div class="col">
                                <a class="btn btn-block u-btn-black g-font-weight-600 g-font-size-12 text-uppercase g-rounded-left-0 g-rounded-right-3 g-px-25 g-py-13" href="{{ url()->previous() }}">
                                    {{ __('Simulate again') }}
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="text-center">
                            <h2 class="h3 g-font-weight-600 g-mb-35">{{ __('Simulate your credit') }}</h2>
                        </div>
                        {!! form($form) !!}

                        <div class="text-center mb-3">
                            <h3 class="h5">{{ __('or') }}</h3>
                        </div>
                    @endif

                    <div class="row no-gutters">
                        <div class="col">
                            <a class="btn btn-block u-btn-twitter g-font-weight-600 g-font-size-12 text-uppercase g-rounded-left-0 g-rounded-right-3 g-px-25 g-py-13" href="{{ route('request',app()->getLocale()) }}">
                                {{ __('Request a loan') }}
                            </a>
                        </div>
                    </div>--}}{{----}}{{--
                </div>--}}{{--
                <!-- End Join Form -->
            </div>--}}
        </div>
    </div>
</div>
<!-- End Promo Block -->
