<!-- Call To Action -->
<section id="go-to-content" class="g-pt-100 g-pb-100 g-bg-secondary">
    <div class="container">
        <!-- Image, Text Block -->
        <div class="row d-flex align-items-lg-center flex-wrap g-mb-60 g-mb-0--lg">
            <div class="col-md-6 col-lg-8">
                <img class="img-fluid rounded" src="{{ url('images/img2_900x600.jpg') }}" alt="Image Description">
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="g-mt-minus-30 g-mt-0--md g-ml-minus-100--lg">
                    <div class="g-mb-20">
                        <h2 class="g-color-red g-font-weight-600 g-font-size-25 g-font-size-35--lg g-line-height-1_2 mb-4">{{ __('Finding the Perfect credit') }}
                        </h2>
                        <p class="g-font-size-16 g-color-main">{{ __('This is where we sit down, grab a cup of coffee and dial in the details. Understanding the loan type you need and plan your request trustly.') }}</p>
                    </div>
                    <a class="btn u-btn-red g-brd-2 g-brd-white g-font-size-13 g-rounded-50 g-pl-20 g-pr-15 g-py-9" href="{{ route('offers',app()->getLocale()) }}">
                        {{ __('Discover our loan offers') }}
                        <span class="align-middle u-icon-v3 g-width-16 g-height-16 g-color-black g-bg-white g-font-size-11 rounded-circle ml-3">
                              <i class="fa fa-angle-right"></i>
                            </span>
                    </a>
                </div>
            </div>
        </div>
        <!-- End Image, Text Block -->
    </div>

    <div class="container">
        <!-- Image, Text Block -->
        <div class="row d-flex justify-content-between align-items-lg-center flex-wrap g-mt-minus-50--lg">
            <div class="col-md-6 order-md-2">
                <div class="g-brd-around--md g-brd-10 g-brd-white rounded">
                    <img class="img-fluid w-100 rounded" src="{{ url('images/img1_600x450.jpg') }}" alt="Image Description">
                </div>
            </div>
            <div class="col-md-6 col-lg-4 ml-auto order-md-1">
                <div class="g-mt-minus-30 g-mt-0--md g-ml-minus-100--lg">
                    <div class="g-mb-20">
                        <h2 class="g-color-red g-font-weight-600 g-font-size-25 g-font-size-35--lg g-line-height-1_2 mb-4">{{ __('A credit that frees you up.') }}
                        </h2>
                        <p class="g-font-size-16 g-color-main">{{ __('You want to finance a project, redo your house, change your car, go around the world... ? With the XTREME CREDIT BANK Personal Loan, borrow the desired amount and define a repayment adapted to your budget.') }}</p>
                    </div>
                    <a class="btn u-btn-red g-brd-2 g-brd-white g-font-size-13 g-rounded-50 g-pl-20 g-pr-15 g-py-9" href="{{ route('request',app()->getLocale()) }}">
                        {{ __('Get a loan') }}
                        <span class="align-middle u-icon-v3 g-width-16 g-height-16 g-color-black g-bg-white g-font-size-11 rounded-circle ml-3">
                              <i class="fa fa-angle-right"></i>
                            </span>
                    </a>
                </div>
            </div>
        </div>
        <!-- End Image, Text Block -->
    </div>
</section>
<!-- End Call To Action -->
