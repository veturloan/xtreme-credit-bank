@extends('layouts.frontend-2')

@section('title')

    {{ __('Credit Card') }}

@endsection

@section('content')

    <section class="g-pt-100 g-mt-100 g-pb-40">
        <div class="container">
            <header class="text-center g-width-80x--md mx-auto g-mb-50">
                <div class="u-heading-v6-2 text-center text-uppercase g-mb-20">
                    <h6 class="g-font-size-12 g-font-weight-600">{{ __('Credit Card') }}</h6>
                    <h2 class="h3 u-heading-v6__title g-brd-primary g-color-gray-dark-v2 g-font-weight-600">{{ __('Discover the XTREME CREDIT BANK credit cards') }}</h2>
                </div>
                <p class="lead">{{ __('Accepted in more than 200 countries, Visa cards offer a secure and reliable way to pay for your needs anywhere in the world. With your Visa card, you can earn cash from more than 2.5 million ATMs worldwide.') }}</p>
                <p class="lead">{{ __('If you are looking to build your credit history, these could be the cards for you. Pay no annual fee or enjoy low interest while still enjoying the ability to shop and collect rewards.') }}</p>
                <p>
                    <a href="{{ route('card.request',app()->getLocale()) }}" class="btn btn-lg" style="background-color: #1B1464; padding-left: 20px; padding-right: 20px; color: #ffffff!important;width: unset!important; border-radius: 5px;margin-bottom: 20px">{{ __('Request a credit card') }}</a>
                </p>
            </header>
        </div>
    </section>
    <!-- End Latest News -->


    <section class="container g-pb-40">
        <div class="row">
            <div class="col-lg-4 g-mb-30">
                <!-- Article -->
                <article class="u-shadow-v1-4">
                    <img class="img-fluid w-100" src="{{ url('images/cards/visa_classic.jpg') }}" alt="Image Description">

                    <div class="g-bg-white g-pa-25">
                        <ul class="list-inline small g-color-gray-dark-v4 g-mb-20">
                            <li class="list-inline-item">
                                <a class="text-uppercase btn btn-xs u-btn-pink rounded-0" href="#">{{ __('Visa Classic') }}</a>
                            </li>
                            <li class="list-inline-item">|</li>
                            <li class="list-inline-item">{{ __('Family') }}</li>
                        </ul>

                        <h3 class="h5 g-font-weight-300 g-mb-40">
                            {{ __('Accepted worldwide is convenient and reliable. Suitable for family expenses, small outings and delicacies.') }}
                        </h3>

                        <div class="media g-font-size-12">
                            <div class="media-body align-self-center">
                                <a class="u-link-v5 text-uppercase g-color-main g-color-primary--hover" href="#">{{ __('more') }}</a>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Article -->
            </div>

            <div class="col-lg-4 g-mb-30">
                <!-- Article -->
                <article class="u-shadow-v1-4">
                    <img class="img-fluid w-100" src="{{ url('images/cards/visa_gold.jpg') }}" alt="Image Description">

                    <div class="g-bg-white g-pa-25">
                        <ul class="list-inline small g-color-gray-dark-v4 g-mb-20">
                            <li class="list-inline-item">
                                <a class="text-uppercase btn btn-xs u-btn-yellow rounded-0" href="#">{{ __('Visa Gold') }}</a>
                            </li>
                            <li class="list-inline-item">|</li>
                            <li class="list-inline-item">{{ __('Business') }}</li>
                        </ul>

                        <h3 class="h5 g-font-weight-300 g-mb-40">
                            {{ __('Is the choice of consumers who want more from their credit card. Good for business and big decisions.') }}
                        </h3>


                        <div class="media g-font-size-12">
                            <div class="media-body align-self-center">
                                <a class="u-link-v5 text-uppercase g-color-main g-color-primary--hover" href="#">{{ __('more') }}</a>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Article -->
            </div>

            <div class="col-lg-4 g-mb-30">
                <!-- Article -->
                <article class="u-shadow-v1-4">
                    <img class="img-fluid w-100" src="{{ url('images/cards/visa_infinite.jpg') }}" alt="Image Description">

                    <div class="g-bg-white g-pa-25">
                        <ul class="list-inline small g-color-gray-dark-v4 g-mb-20">
                            <li class="list-inline-item">
                                <a class="text-uppercase btn btn-xs u-btn-darkpurple rounded-0" href="#">{{ __('Visa Infinite') }}</a>
                            </li>
                            <li class="list-inline-item">|</li>
                            <li class="list-inline-item">{{ __('Top line') }}</li>
                        </ul>

                        <h3 class="h5 g-font-weight-300 g-mb-40">
                            {{ __('An excellent card that enriches your passions. Challenge the laws of expenditure. Do not think anymore, immerse yourself in infinite pleasure.') }}
                        </h3>


                        <div class="media g-font-size-12">
                           <div class="media-body align-self-center">
                                <a class="u-link-v5 text-uppercase g-color-main g-color-primary--hover" href="#">{{ __('more') }}</a>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- End Article -->
            </div>
        </div>
    </section>

@endsection
