<section class="g-pt-100 g-pb-100" style="background-color: #ffffff!important;">
<!-- Partners -->
<div class="container">
    <!-- Heading -->
    <div class="g-overflow-hidden text-center">
        <div class="u-info-v10-1">
            <h3 class="h6 g-font-weight-600 g-color-red text-center text-uppercase mb-0">{{ __('Our trusted partners') }}</h3>
        </div>
    </div>
    <!-- End Heading -->

    <div id="carouselCus2" class="js-carousel u-carousel-v12 g-mt-20"
         data-infinite="true"
         data-is-thumbs="true"
         data-autoplay="true"
         data-speed="10000"
         data-center-mode="true"
         data-slides-show="5"
         data-nav-for="#carouselCus1"
         data-responsive='[{
               "breakpoint": 1200,
               "settings": {
                 "slidesToShow": 5
               }
             }, {
               "breakpoint": 992,
               "settings": {
                 "slidesToShow": 4
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "slidesToShow": 3
               }
             }, {
               "breakpoint": 576,
               "settings": {
                 "slidesToShow": 3
               }
             }, {
               "breakpoint": 446,
               "settings": {
                 "slidesToShow": 2
               }
             }]'>
        <div class="js-slide u-block-hover">
            <img class="mx-auto g-width-150" src="{{ url('images/partners/1.jpg') }}" alt="Image description">
        </div>

        <div class="js-slide u-block-hover">
            <img class="mx-auto g-width-150" src="{{ url('images/partners/2.jpg') }}" alt="Image description">
        </div>

        <div class="js-slide u-block-hover">
            <img class="mx-auto g-width-150" src="{{ url('images/partners/3.jpg') }}" alt="Image description">
        </div>

        <div class="js-slide u-block-hover">
            <img class="mx-auto g-width-150" src="{{ url('images/partners/4.jpg') }}" alt="Image description">
        </div>

        <div class="js-slide u-block-hover">
            <img class="mx-auto g-width-150" src="{{ url('images/partners/5.jpg') }}" alt="Image description">
        </div>
    </div>
</div>
<!-- End Partners -->
</section>
