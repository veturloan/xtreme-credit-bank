@extends('layouts.frontend-2')

@section('title')

    {{ __('Contact Us') }}

@endsection

@section('content')

    <!-- Promo Block -->
    <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall " data-options='{direction: "fromtop", animation_duration: 25, direction: "reverse"}'>
        <!-- Parallax Image -->
        <div class="divimage dzsparallaxer--target w-100 g-bg-cover g-bg-size-cover g-bg-pos-top-center g-bg-black-opacity-0_2--after" style="height: 140%; background-image: url({{ url('images/img2.jpg') }});"></div>
        <!-- End Parallax Image -->

        <!-- Promo Block Content -->
        <div class="container g-color-white text-center g-pt-150 g-pb-200">
            @if(session('success'))
                <div class="alert alert-success">
                    <h4 class="h4">{{ session('success') }}</h4>
                </div>
            @else
                <h3 class="h2 g-font-weight-300 mb-0">{{ __('You came to the right place') }}</h3>
                <h2 class="g-font-weight-700 g-font-size-80 text-uppercase">{{ __("Let's Talk") }}</h2>
            @endif
        </div>
        <!-- Promo Block Content -->
    </section>
    <!-- End Promo Block -->

    <!-- Contact Form -->
    <section class="container">
        <!-- Icon Blocks -->
        <div class="row no-gutters u-shadow-v21 g-mt-minus-100">
            <div class="col-sm-6 col-md-4 g-brd-right--md g-brd-gray-light-v4">
                <!-- Icon Blocks -->
                <div class="g-bg-white text-center g-py-100">
            <span class="u-icon-v1 u-icon-size--xl g-color-primary mb-3">
                <i class="icon-real-estate-027 u-line-icon-pro"></i>
              </span>
                    <h4 class="h5 g-font-weight-600 g-mb-5">{{ __('Address') }}</h4>
                    <span class="d-block">Richtstrasse 16, 7007 Chur, Coire, Suisse</span>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-sm-6 col-md-4 g-hidden-xs-down g-brd-right--md g-brd-gray-light-v4">
                <!-- Icon Blocks -->
                <div class="g-bg-white text-center g-py-100">
            <span class="u-icon-v1 u-icon-size--xl g-color-primary mb-3">
                <i class="icon-electronics-005 u-line-icon-pro"></i>
              </span>
                    <h4 class="h5 g-font-weight-600 g-mb-5">{{ __('Phone Number') }}</h4>
                    <span class="d-block">{{ env('SITE_PHONE') }}</span>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-sm-6 col-md-4 g-hidden-sm-down">
                <!-- Icon Blocks -->
                <div class="g-bg-white text-center g-py-100">
            <span class="u-icon-v1 u-icon-size--xl g-color-primary mb-3">
                <i class="icon-communication-062 u-line-icon-pro"></i>
              </span>
                    <h4 class="h5 g-font-weight-600 g-mb-5">{{ __('Email') }}</h4>
                    <span class="d-block">{{ env('SITE_CONTACT_EMAIL') }}</span>
                </div>
                <!-- End Icon Blocks -->
            </div>
        </div>
        <!-- End Icon Blocks -->

        <div class="g-py-100">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <h3 class="g-color-black g-font-weight-600 text-center mb-5">{{ __('Who are you, and how can we help?') }}</h3>
                    {!! form_start($form) !!}
                    <div class="row">
                        {!! form_rows($form,['last_name','first_name','email','phone','subject','message']) !!}
                    </div>
                    {!! form_rest($form) !!}
                    {!! form_end($form) !!}
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Form -->

@endsection
