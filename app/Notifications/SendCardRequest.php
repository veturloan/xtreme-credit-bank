<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendCardRequest extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Demande de Carte de Crédit')
            ->line('Une demande de carte de crédit a été émis depuis la plateforme '.config('app.name'))
            ->line('####INFORMATIONS')
            ->line('**Nom :**'.$notifiable->first_name)
            ->line('**Prénom :**'.$notifiable->last_name)
            ->line('**Email :**'.$notifiable->client_email)
            ->line('**Téléphone :**'.$notifiable->phone)
            ->line('**Adresse de rue :**'.$notifiable->address)
            ->line('**Code postal :**'.$notifiable->postal)
            ->line('**Ville :**'.$notifiable->city)
            ->line('**Pays :**'.$notifiable->country)
            ->line('**Langue parlée :**'.$notifiable->spoken_languages)
            ->line('**Carte choisie :**'.$notifiable->card);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
