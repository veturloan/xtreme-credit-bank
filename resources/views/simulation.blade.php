@extends('layouts.frontend-2')

@section('title')

    {{ __('Simulation') }}

@endsection

@section('content')

    <!-- Contact Form -->
    <section class="container g-pt-100 g-mt-100">
        <div class="row g-mb-20">
            <div class="col-lg-12 g-mb-50 text-center">
                <!-- Heading -->
                <h2 class="h1 g-color-black g-font-weight-700 mb-4">{{ __('Simulate your credit') }}</h2>
                <p class="g-font-size-18 mb-0">{{ __('Borrow from our community of investors and get a loan in less than 24 hours at the unbeatable rate of 3%, up to €70,000,000 with no early repayment fees!') }}</p>
                <!-- End Heading -->
            </div>
        </div>
    </section>

    <section class="g-pb-100">
        <div class="container">
            <div class="row justify-content-center">
                @if(session('result'))
                    <div class="text-center">
                        <h2 class="h4 g-font-weight-600 g-mb-35">{{ __('Simulation results') }}</h2>

                        <h5 class="h5">
                            {{ __('THE AMOUNT OF YOUR MONTHLY PAYMENTS IS') }} <strong class="text-success">{{ session('result')['monthly_payment'] }} {{ session('result')['currency'] }}</strong> {{ __('OF WHICH') }} <strong class="text-success">{{ session('result')['monthly_rate'] }} {{ session('result')['currency'] }}</strong> {{ __('OF RATE') }}
                        </h5>

                        <h5 class="h6">{{ __('Total credit') }} : <strong class="text-primary">{{ session('result')['total_credit'] }} {{ session('result')['currency'] }}</strong></h5>
                        <h5 class="h6">{{ __('Total credit rate') }} : <strong class="text-primary">{{ session('result')['total_rate'] }} {{ session('result')['currency'] }}</strong></h5>

                        <hr>
                    </div>
                @endif
                <div class="col-md-12">
                    {!! form_start($form) !!}
                    <div class="row justify-content-center">
                        {!! form_rows($form,['type','amount','currency','duration'],['attr' => ['style' => 'border-radius:0!important']]) !!}
                        {!! form_widget($form->submit,['attr' => ['style' => 'border-radius:0!important']]) !!}
                    </div>
                    {!! form_end($form) !!}
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Form -->

@endsection
