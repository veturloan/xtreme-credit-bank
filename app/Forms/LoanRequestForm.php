<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class LoanRequestForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('last_name','text',[
                'label' => __('Last Name'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your last name...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('first_name','text',[
                'label' => __('First Name'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your first name...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('gender','select',[
                'choices' => get_gender_list(),
                'empty_value' => '==='.__('Choose your gender').'===',
                'label' => __('Sex'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'class' => 'js-custom-select w-100 u-select-v2 u-shadow-v19 g-color-black g-color-primary--hover g-bg-white text-left g-rounded-30 g-px-15 g-py-5'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('age','number',[
                'label' => __('Age'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your age...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'numeric',
                    'max:100'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('profession','text',[
                'label' => __('Profession'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your profession...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('country','text',[
                'label' => __('Country'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your country...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('city','text',[
                'label' => __('City'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your city...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('address','text',[
                'label' => __('Address'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your address...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('postal_code','text',[
                'label' => __('Postal code'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your postal code...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('email','email',[
                'label' => __('E-mail Address'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your e-mail address...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'email',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('phone','text',[
                'label' => __('Telephone'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your phone number...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'numeric',
                    'digits_between:7,250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('type', 'select',[
                'choices' => get_loan_types(),
                'empty_value' => '==='.__('Choose credit type').'===',
                'label' => __('Credit type'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'class' => 'js-custom-select w-100 u-select-v2 u-shadow-v19 g-color-black g-color-primary--hover g-bg-white text-left g-rounded-30 g-px-15 g-py-5'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('amount','text',[
                'label' => __('Amount requested'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Amount of credit requested...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'numeric',
                    'digits_between:4,9'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('currency','select',[
                'choices' => config('currencies_list.list'),
                'empty_value' => '=== '.__('Select currency').' ===',
                'label' => __('Currency'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'class' => 'js-custom-select w-100 u-select-v2 u-shadow-v19 g-color-black g-color-primary--hover g-bg-white text-left g-rounded-30 g-px-15 g-py-5'
                ],
                'rules' => [
                    'required',
                    'string',
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('duration', 'select',[
                'choices' => get_loan_durations(),
                'empty_value' => '==='.__('Choose credit duration').'===',
                'label' => __('Credit duration'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'class' => 'js-custom-select w-100 u-select-v2 u-shadow-v19 g-color-black g-color-primary--hover g-bg-white text-left g-rounded-30 g-px-15 g-py-5'
                ],
                'rules' => [
                    'required',
                    'string'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('monthly_expenses','text',[
                'label' => __('Estimate of your monthly expenses'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Write the total amount of your monthly expenses...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'numeric',
                    'digits_between:1,9'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('have_another_credit','select',[
                'choices' => [
                    'yes' => __('yes'),
                    'no' => __('no'),
                ],
                'empty_value' => '==='.__('Choose Yes or No').'===',
                'label' => __('Do you have another credit in progress?'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'class' => 'js-custom-select w-100 u-select-v2 u-shadow-v19 g-color-black g-color-primary--hover g-bg-white text-left g-rounded-30 g-px-15 g-py-5'
                ],
                'rules' => [
                    'required',
                    'string',
                    'alpha',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('account_number','text',[
                'label' => __('Account Number'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your account number...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add('bank_name','text',[
                'label' => __('Bank Name'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your Bank name...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-4 form-group g-mb-15'
                ]
            ])
            ->add(__('Continue'), 'submit',[
                'attr' => [
                    'class' => 'btn u-btn-black g-brd-main g-brd-primary--hover g-bg-main g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-px-100 g-py-13'
                ],
                'wrapper' => [
                    'class' => 'text-center'
                ]
            ]);
    }
}
