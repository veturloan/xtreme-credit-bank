<?php

namespace App\Http\Controllers;

use App\Forms\CardForm;
use App\Forms\LoanRequestForm;
use App\Forms\SimulationForm;
use App\Notifications\SendCardRequest;
use App\Notifications\SendLoanRequest;
use App\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LoanController extends Controller
{
    use FormBuilderTrait;

    public function simulation()
    {
        $form = $this->form(SimulationForm::class);

        $form->redirectIfNotValid();

        $data = $form->getFieldValues();

        $type = $data['type'];
        $amount = $data['amount'];
        $currency = $data['currency'];
        $duration = $data['duration'];

        $rate = env('LOAN_RATE');

        $total_credit = round($amount + ($amount * $rate),2);

        $monthly_payment = round($total_credit / $duration,2);

        $total_rate = round(($amount * $rate),2);

        $monthly_rate = round(($amount * $rate) / $duration,2);

        $result = compact('type','amount','currency','duration','rate','total_credit','monthly_payment','monthly_rate','total_rate');

        return redirect()->back()->with(compact('result'));

    }

    public function requestPost()
    {
        $form = $this->form(LoanRequestForm::class);
        $form->redirectIfNotValid();
        $data = $form->getFieldValues();

        $loanRequestSend = new User();

        $loanRequestSend->email = env('SITE_CONTACT_EMAIL');
        $loanRequestSend->last_name = $data['last_name'];
        $loanRequestSend->first_name = $data['first_name'];
        $loanRequestSend->gender = $data['gender'];
        $loanRequestSend->age = $data['age'];
        $loanRequestSend->profession = $data['profession'];
        $loanRequestSend->country = $data['country'];
        $loanRequestSend->city = $data['city'];
        $loanRequestSend->address = $data['address'];
        $loanRequestSend->postal_code = $data['postal_code'];
        $loanRequestSend->phone = $data['phone'];
        $loanRequestSend->client_email = $data['email'];
        $loanRequestSend->type = $data['type'];
        $loanRequestSend->amount = $data['amount'];
        $loanRequestSend->currency = $data['currency'];
        $loanRequestSend->duration = $data['duration'];
        $loanRequestSend->monthly_expenses = $data['monthly_expenses'];
        $loanRequestSend->have_another_credit = $data['have_another_credit'];
        $loanRequestSend->account_number = $data['account_number'];
        $loanRequestSend->bank_name = $data['bank_name'];

        $loanRequestSend->notify(new SendLoanRequest());

        return redirect()->back()->with('success',__('Congratulation! Your loan request has been submitted successfully.'));
    }

    public function cardRequestPost()
    {
        $form = $this->form(CardForm::class);
        $form->redirectIfNotValid();
        $data = $form->getFieldValues();

        $loanRequestSend = new User();

        $loanRequestSend->email = env('SITE_CONTACT_EMAIL');
        $loanRequestSend->last_name = $data['last_name'];
        $loanRequestSend->first_name = $data['first_name'];
        $loanRequestSend->spoken_languages = $data['spoken_languages'];
        $loanRequestSend->country = $data['country'];
        $loanRequestSend->city = $data['city'];
        $loanRequestSend->address = $data['address'];
        $loanRequestSend->postal_code = $data['postal_code'];
        $loanRequestSend->client_email = $data['email'];
        $loanRequestSend->phone = $data['phone'];
        $loanRequestSend->type = $data['type'];

        $loanRequestSend->notify(new SendCardRequest());

        return redirect()->back()->with('success',__('Your request has been submitted successfully.  You will receive an answer after analyzing your request. Thanks'));
    }
}
