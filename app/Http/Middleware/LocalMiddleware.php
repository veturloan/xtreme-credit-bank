<?php

namespace App\Http\Middleware;

use Closure;

class LocalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*dd($request->route('lang'));
        if (session()->has('locale')){
            $locale = session()->get('locale',config('app.locale'));
        } else {
            $locale = $request->route('lang');
            session()->put('locale',$locale);
        }*/
        $locale = $request->route('lang');
        session()->put('locale',$locale);

        app()->setLocale($locale);
        return $next($request);
    }
}
