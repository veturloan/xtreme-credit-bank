<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SimulationForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('type', 'select',[
                'choices' => get_loan_types(),
                'label_show' => false,
                'attr' => [
                    'class' => 'form-control rounded g-px-20 g-py-12 mb-3'
                ],
                'empty_value' => '==='.__('Choose credit type').'===',
                'rules' => [
                    'required'
                ]
            ])
            ->add('amount', 'text',[
                'label_show' => false,
                'attr' => [
                    'class' => 'form-control rounded g-px-20 g-py-11 mb-3',
                    'placeholder' => __('Desired amount')
                ],
                'rules' => [
                    'required',
                    'numeric',
                    'digits_between:4,9',
                ]
            ])
            ->add('currency', 'select',[
                'choices' => config('currencies_list.list'),
                'label_show' => false,
                'attr' => [
                    'class' => 'form-control rounded g-px-20 g-py-12 mb-3'
                ],
                'empty_value' => '==='.__('Choose your currency').'===',
                'rules' => [
                    'required'
                ]
            ])
            ->add('duration', 'select',[
                'choices' => get_loan_durations(),
                'label_show' => false,
                'attr' => [
                    'class' => 'form-control rounded g-px-20 g-py-12 mb-3'
                ],
                'empty_value' => '==='.__('Credit duration').'===',
                'rules' => [
                    'required'
                ]
            ])
            ->add('submit', 'submit',[
                'label' => __('Continue'),
                'attr' => [
                    'class' => 'btn btn-block u-btn-black g-brd-main g-brd-primary--hover g-bg-main g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-px-25 g-py-13'
                ],
                'wrapper' => [
                    'class' => 'g-mb-35'
                ]
            ]);
    }
}
