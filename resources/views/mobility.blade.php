@extends('layouts.frontend-2')

@section('title')

    {{ __('Banking Mobility') }}

@endsection

@section('content')

    <!-- Section -->
    <section class="container g-pt-100 g-pb-40 g-mt-100">
        <div class="row">
            <div class="col-lg-6 g-mb-60">
                <div class="mb-4">
                    <h2 class="h3 text-uppercase mb-3">{{ __('A new Level') }}</h2>
                    <div class="g-width-60 g-height-1 g-bg-black"></div>
                </div>
                <div class="mb-5">
                    <p>{{ __('Your money is following you everywhere and safely.') }}</p>
                    <p>{{ __('Should anything ever happen, we’ve got you covered. We’ll fully reimburse you for any unauthorized transactions3 made through the XTREME CREDIT BANK Mobile app or XTREME CREDIT BANK Online Banking.') }}</p>
                    <p>{{ __('We’re at the forefront of cyber-security and use the latest and most advanced technology to keep your information secure—no matter when or how you bank.') }}</p>
                </div>
                <a class="btn btn-xl u-btn-outline-black g-font-size-default mr-2" href="{{ route('card',app()->getLocale()) }}">{{ __('Discover the XTREME CREDIT BANK credit cards') }}</a>
            </div>

            <div class="col-lg-6 g-mb-60">
                <img src="{{ url('images/ATM.jpg') }}" class="img-fluid">
            </div>
        </div>
    </section>
    <!-- End Section -->

    <!-- Icon Blocks -->
    <section class="clearfix">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <!-- Icon Blocks -->
                <div class="g-bg-pink g-color-white text-center g-py-100 g-px-50">
            <span class="u-icon-v2 g-rounded-5 g-mb-25">
              <i class="icon-education-087 u-line-icon-pro"></i>
            </span>
                    <h3 class="h4 g-font-weight-600 mb-30">{{ __('XTREME CREDIT BANK Mobile') }}</h3>
                    <p class="g-color-white-opacity-0_8">{{ __('Pay a bill, check your balances, send money, deposit a cheque, and more, all from your mobile phone.') }}</p>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-lg-4">
                <!-- Icon Blocks -->
                <div class="g-bg-teal g-color-white text-center g-py-100 g-px-50">
            <span class="u-icon-v2 g-rounded-5 g-mb-25">
              <i class="icon-education-035 u-line-icon-pro"></i>
            </span>
                    <h3 class="h4 g-font-weight-600 mb-30">{{ __('XTREME CREDIT BANK Wallet') }}</h3>
                    <p class="g-color-white-opacity-0_8">{{ __('Pay for purchases up to $100 with a tap of your phone. Plus send money, and keep your gift cards in one place.') }}</p>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-lg-4">
                <!-- Icon Blocks -->
                <div class="g-bg-purple g-color-white text-center g-py-100 g-px-50">
            <span class="u-icon-v2 g-rounded-5 g-mb-25">
              <i class="icon-education-141 u-line-icon-pro"></i>
            </span>
                    <h3 class="h4 g-font-weight-600 mb-30">{{ __('XTREME CREDIT BANK Rewards') }}</h3>
                    <p class="g-color-white-opacity-0_8">{{ __('Check your points balance, book a flight, redeem for gift cards or merchandise, all in one convenient app.') }}</p>
                </div>
                <!-- End Icon Blocks -->
            </div>
        </div>
    </section>
    <!-- End Icon Blocks -->

    <!-- Research Article -->
    <div class="g-bg-img-hero" style="background-image: url({{ url('assets2/include/svg/svg-bg3.svg') }});">
        <div class="container g-py-100">
            <div class="row justify-content-lg-between align-items-center">
                <div class="col-md-6 g-mb-50 g-mb-0--md">
                    <img class="img-fluid u-shadow-v39 g-brd-around g-brd-10 g-brd-white rounded" src="{{ url('images/banners/1.jpg') }}" alt="Image Description">
                </div>

                <div class="col-md-5">
                    <div class="mb-4">
                        <h1 class="g-color-black mb-3">{{ __('Bank, Shop, Pay and Redeem') }}</h1>
                        <p>{{ __('Check your balances, pay with a tap of your mobile device, redeem XTREME CREDIT BANK Rewards points and more. XTREME CREDIT BANK has an app to manage your banking needs when it’s most convenient for you.') }}</p>
                    </div>
                    <a class="btn u-shadow-v33 g-color-white g-bg-primary g-bg-black--hover g-font-size-default g-rounded-30 g-px-35 g-py-11" href="{{ env('BANK_SOFT_URL') }}">{{ __('open an account') }}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Research Article -->

@endsection
