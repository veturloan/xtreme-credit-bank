<div class="u-header__section u-header__section--light g-bg-white g-py-10" data-header-fix-moment-exclude="g-bg-white g-py-10" data-header-fix-moment-classes="g-bg-white u-shadow-v18 g-py-0" style="background-color: #ffeedd!important;">
    <nav class="js-mega-menu navbar navbar-expand-lg">
        <div class="container">
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Logo -->
            <a href="{{ route('welcome',app()->getLocale()) }}" class="navbar-brand">
                <img class="g-width-180" src="{{ url('images/logo/logo-dark.png') }}" alt="Image Description">
            </a>
            <!-- End Logo -->

            <!-- Navigation -->
            <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg" id="navBar">
                <ul class="navbar-nav text-uppercase g-font-weight-700 ml-auto">
                    <li class="nav-item g-mx-20--lg">
                        <a href="{{ route('welcome',app()->getLocale()) }}" class="nav-link px-0">{{ __('home') }}</a>
                    </li>

                    <li class="nav-item hs-has-sub-menu g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut">
                        <a id="nav-link--blog" class="nav-link g-py-7 g-px-0" href="#!" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu--blog">{{ __('banking services') }}</a>

                        <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-20 g-mt-10--lg--scrolling animated fadeOut" id="nav-submenu--blog" aria-labelledby="nav-link--blog" style="display: none;">
                            <li class="pl-3">
                                <a href="{{ route('mobility',app()->getLocale()) }}" class="nav-link px-0">{{ __('banking mobility') }}</a>
                            </li>
                            <li class="pl-3">
                                <a href="{{ route('card',app()->getLocale()) }}" class="nav-link px-0">{{ __('credit card') }}</a>
                            </li>
                            <li class="pl-3">
                                <a href="{{ route('insurance',app()->getLocale()) }}" class="nav-link px-0">{{ __('Insurance') }}</a>
                            </li>
                        </ul>
                    </li>
                        <!-- Mega Menu Item -->
                    <li class="hs-has-mega-menu nav-item g-mx-20--lg" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="50%" data-position="center">
                        <a id="mega-menu-label-1" class="nav-link g-px-0" href="{{ route('offers',app()->getLocale()) }}" aria-haspopup="true" aria-expanded="false">{{ __('Credit offers') }}
                            <i class="hs-icon hs-icon-arrow-bottom g-font-size-11 g-ml-7"></i>
                        </a>
                        <!-- Mega Menu -->
                        <div class="hs-mega-menu u-shadow-v11 g-text-transform-none font-weight-normal g-brd-top g-brd-primary g-brd-top-2 g-mt-17 g-mt-7--lg--scrolling" aria-labelledby="mega-menu-label-1">
                            <div class="row align-items-stretch">
                                <div class="col-lg-4">
                                    <section class="g-pa-20">
                                        <ul class="list-unstyled">
                                            @foreach(config('loan_types.list') as $k => $v)
                                                @if(in_array($k,config('loan_types.column_1')))
                                                    <li>
                                                        <a class="nav-link g-px-0 g-color-primary--hover" href="{{ route('offers',app()->getLocale()) }}">
                                                            {{ __($k) }}
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </section>
                                </div>
                                <div class="col-lg-4 g-brd-left--lg g-brd-gray-light-v5">
                                    <section class="g-pa-20">
                                        <ul class="list-unstyled">
                                            @foreach(config('loan_types.list') as $k => $v)
                                                @if(in_array($k,config('loan_types.column_2')))
                                                    <li>
                                                        <a class="nav-link g-px-0 g-color-primary--hover" href="{{ route('offers',app()->getLocale()) }}">
                                                            {{ __($k) }}
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </section>
                                </div>
                                <div class="col-lg-4 g-brd-left--lg g-brd-gray-light-v5">
                                    <section class="g-pa-20 g-pl-5--lg">
                                        <ul class="list-unstyled">
                                            @foreach(config('loan_types.list') as $k => $v)
                                                @if(in_array($k,config('loan_types.column_3')))
                                                    <li>
                                                        <a class="nav-link g-px-0 g-color-primary--hover" href="{{ route('offers',app()->getLocale()) }}">
                                                            {{ __($k) }}
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <!-- End Mega Menu -->
                    </li>

                    <li class="nav-item g-mx-20--lg">
                        <a href="{{ route('request',app()->getLocale()) }}" class="nav-link px-0">{{ __('Make a Request') }}</a>
                    </li>
                </ul>
            </div>
            <!-- End Navigation -->
        </div>
    </nav>
</div>
