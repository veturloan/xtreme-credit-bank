<?php

return [
    'list' => [
        '€' => 'Euro (€)',
        '$' => 'US Dollar ($)',
        'lei' => 'RON (lei)',
        'S/.' => 'PEN (S/.)',
        '₽' => 'RUB (₽)',
        '₮' => 'MNT (₮)',
        '₸' => 'KZT (₸)',
        '฿' => 'THB (฿)',
    ]
];
