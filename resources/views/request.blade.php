@extends('layouts.frontend-2')

@section('title')

    {{ __('Loan request') }}

@endsection

@section('content')

    <!-- Contact Form -->
    <section class="container g-pt-100 g-mt-100">
        <div class="row g-mb-20">
            <div class="col-lg-6 g-mb-50">
                <!-- Heading -->
                <h2 class="h1 g-color-black g-font-weight-700 mb-4">{{ __('A loan adapted to every need') }}</h2>
                <p class="g-font-size-18 mb-0">{{ __('Borrow from our community of investors and get a loan in less than 24 hours at the unbeatable rate of 1%, up to €70,000,000 with no early repayment fees!') }}</p>
                <!-- End Heading -->
            </div>
            <div class="col-lg-6 align-self-center ml-auto g-mb-50">
                <div class="media">
                    <div class="d-flex align-self-center">
                        <span class="u-icon-v2 u-icon-size--sm g-color-white g-bg-primary rounded-circle mr-3">
                            <i class="g-font-size-16 fa fa-exclamation-triangle"></i>
                        </span>
                    </div>
                    <div class="media-body align-self-center">
                        <h3 class="h6 g-color-black g-font-weight-700 text-uppercase mb-0">{{ __('Warning') }}</h3>
                        <p class="mb-0">
                            {{ __('A credit commits you and must be repaid. Check your ability to repay before committing.') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="g-pb-100">
        <div class="container">
            <div class="row justify-content-center">
                @if(session('success'))
                    <div class="alert alert-success">
                        <h5>{{ session('success') }}</h5>
                    </div>
                @endif
                <div class="col-md-12">
                    {!! form_start($form) !!}
                    <h4 class="h5 text-uppercase">{{ __('Personal Data') }}</h4>
                    <hr class="g-mt-0">
                    <div class="row">
                        {!! form_rows($form,['first_name','last_name','gender','age','profession']) !!}
                    </div>
                    <h4 class="h5 text-uppercase">{{ __('Contact details') }}</h4>
                    <hr class="g-mt-0">
                    <div class="row">
                        {!! form_rows($form,['country','city','address','postal_code','email','phone']) !!}
                    </div>
                    <h4 class="h5 text-uppercase">{{ __('Credit information') }}</h4>
                    <hr class="g-mt-0">
                    <div class="row">
                        {!! form_rows($form,['type','amount','currency','duration','monthly_expenses','have_another_credit','account_number','bank_name']) !!}
                    </div>
                    {!! form_end($form) !!}
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Form -->

@endsection
