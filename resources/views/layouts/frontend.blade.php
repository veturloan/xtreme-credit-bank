<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Title -->
    <title>{{ config('app.name') }} | @yield('title',__('a credit that makes you enjoy'))</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('favicon.png') }}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:400,500,600" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ url('css/all.css') }}">

    <style>

        body {
            background-color: #eaeaea!important;
        }

        .no-border-radius {
            border-radius: unset!important;
        }

        .no-border {
            border: unset!important;
        }

        .account-btn {
            background-color: #FB8500;border:unset!important;
        }
        .account-btn:hover {
            background-color: #ffffff;color:#FB8500!important;
        }
        .login-btn {
            background-color: #ffffff;border: unset!important;color: #FB8500;border-radius: unset!important;
        }
        .login-btn:hover {
            background-color: #FB8500;color:#ffffff!important;
        }
        .g-color-red {
            color:#219EBC!important;
        }
        .u-btn-red, .g-bg-red {
            background-color: #219EBC!important;color:#ffffff!important;
        }

        .u-header__section--light .navbar-nav:not([class*=u-main-nav-v]) .nav-link, .u-header__section--light--shift.js-header-change-moment .navbar-nav:not([class*=u-main-nav-v]) .nav-link {
            color: #FB8500!important;
        }

        .g-bg-blue {
            background-color: #FB8500!important;
        }


    </style>

</head>

<body>
<main>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--sticky-top u-header--toggle-section u-header--change-appearance" data-header-fix-moment="300">
        @include('includes.top-bar')

        @include('includes.menu')
    </header>
    <!-- End Header -->

    @yield('content')

    @include('includes.footer')

</main>
<script src="{{ url('js/all.js') }}"></script>
@yield('scripts')
</body>
</html>

