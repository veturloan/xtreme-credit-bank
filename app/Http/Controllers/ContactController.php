<?php

namespace App\Http\Controllers;

use App\Forms\ContactForm;
use App\Notifications\SendContact;
use App\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ContactController extends Controller
{
    use FormBuilderTrait;

    public function contactPost()
    {
        $form = $this->form(ContactForm::class);

        $form->redirectIfNotValid();

        $data = $form->getFieldValues();

        $sendContact = new User();
        $sendContact->email = env('SITE_CONTACT_EMAIL');
        $sendContact->contact_first_name = $data['first_name'];
        $sendContact->contact_last_name = $data['last_name'];
        $sendContact->contact_email = $data['email'];
        $sendContact->contact_phone = $data['phone'];
        $sendContact->contact_subject = $data['subject'];
        $sendContact->contact_message = $data['message'];
        $sendContact->notify(new SendContact());

        return redirect()->back()->with('success',__('Your message has been successfully sent! We will provide you with an answer within a short period of time. Thank you!'));
    }
}
