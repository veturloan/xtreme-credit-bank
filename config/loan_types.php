<?php

return [
    'list' => [
        'Financial' => "100% online financing adapted to professionals. Easier and faster than banks. Geld-Finances allows you to borrow from €20,000 to €500,000 in record time.",
        'Real estate loans' => "Start a real estate project, a first purchase, a rental investment, the purchase of a second home or a home loan buyback.",
        'Investment loan' => "Investment credit is a credit that allows the company to make professional investments in the medium or long term. Investment credit generally corresponds to the financing of the company's equity capital, with the main objective of developing or renewing the company's fixed assets and working tools.",
        'Car loan' => "Would you like to buy a new car by taking out a car loan? Geld-Finances supports you in this personal project whether you are buying a new or used car.",
        'Consolidation debt' => "A debt consolidation loan can be used to pay off credit card debts, pay overdue bills, car loans and much more. When you repay your debts with a consolidation loan, you only have to repay the new loan.",
        'Line of credit' => "A line of credit corresponds to an open, available credit, the maximum amount and repayment terms of which are defined by a contract between a bank and a company.",
        'Mortgage loan' => "A mortgage loan is a loan granted under the condition of mortgaging one or more real estate assets constituting the borrower's assets.",
        'Repurchase of credit' => "The principle of credit repurchase (or grouping of credits) is to group all your credits into one. This simplifies the management of your budget and allows you to review the amount of your monthly payments and the duration of your credits.",
        'Consumer loan' => "Do you want to decorate or renovate your home with a renovation loan? Do you want to go on holiday with a travel credit?",
        'Bank Credit' => "Bank loans are financing granted to various economic agents (legal entities or natural persons) by credit institutions. Before they are granted, they involve a risk analysis and also the taking of guarantees.",
        'Agreement loans' => "The main difference between a conventional loan and other types of mortgage loans is that a conventional loan is not made by a government entity or insured by a government entity. This is what we call a non-GSE loan. A non-governmental entity.",
        'Conventional loan' => "Conventional loans are those contracted for more or less important acquisitions: cars, household equipment, everyday expenses... For the borrower facing a significant financial need, the amount of money can effectively help him to make the purchase or optimize his budget.",
        'Loan works' => "Need to renovate your home or your exterior? To support you in this project, Geld-Finances allows you, thanks to the Personal Works Loan, to benefit from a fixed rate. To get a better idea of what this project will cost you, do your simulations online: it's free, fast and without obligation.",
        'Social loan' => "As its name suggests, the social loan is a type of loan designed to help families or individuals with low incomes, whether through low-paid work or other means, to carry out housing projects, with a fixed rate determined according to the income of each household.",
        'Bridge loan' => "A bridging loan is a short-term loan that allows its subscriber to acquire a property before closing the sale of another. This transitional credit is an interesting alternative for homeowners who want to live in a more spacious or better located home.",
        'In Fine Loan' => "Unlike a depreciable loan, it is a loan for which you only pay interest during its term. The capital is not amortized but is repaid in a single instalment at maturity.",
        'Regulated loan' => "A regulated loan complies with strict conditions defined by the government. Thus, the bank must scrupulously apply the conditions of granting and the tariffs set by the regulations.",
        'Free loan' => "This type of bank loan is called \"free\" because it is not subject to government regulations, neither at the level of the subscriber, the financial package, nor with regard to the asset financed. It is opposed to regulated credit, which must comply with a large number of rules.",
        'Housing loan' => "A real estate loan is a long-term loan granted by a credit institution to an individual to finance the acquisition of a home.",
        'Amortization loan' => "The amortization of a loan (bank or bond) is the part of the capital that is repaid at each periodic maturity (e.g. each month). This payment is made at the same time as the interest due for the same period.",
        'Accession Assistance Loan' => "It differs from the traditional conventional conventional conventional loan (PC) in that it is intended to finance the main residence (new or old housing) of low-income households who can, under resource conditions, benefit from Personalized Housing Assistance (PHA).",
        'Modular loan' => "The flexible loan anticipates these potential changes by allowing the borrower to manage their loan by having the possibility to modify their repayments, under certain conditions.",
        'Revisable rate loan' => "A variable rate loan is a loan indexed to an interest rate which, unlike a fixed rate, can vary over the life of the loan, depending on financial market conditions.",
    ],

    'featured' => [
        'Financial',
        'Real estate loans',
        'Car loan',
        'Bank Credit',
        'Consumer loan',
        'Mortgage loan',
    ],
    'column_1' => [
        'Financial',
        'Real estate loans',
        'Investment loan',
        'Car loan',
        'Consolidation debt',
        'Line of credit',
        'Mortgage loan',
        'Repurchase of credit'
    ],
    'column_2' => [
        'Consumer loan',
        'Bank Credit',
        'Agreement loans',
        'Conventional loan',
        'Loan works',
        'Social loan',
        'Bridge loan',
        'In Fine Loan'
    ],
    'column_3' => [
        'Regulated loan',
        'Free loan',
        'Housing loan',
        'Amortization loan',
        'Accession Assistance Loan',
        'Modular loan',
        'Revisable rate loan'
    ],
];
