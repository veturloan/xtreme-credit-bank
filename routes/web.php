<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('welcome',Request()->getLocale());
});

Route::prefix('{lang}')->group(function () {
    Route::get('home', 'FrontEndController@welcome')->name('welcome');
    Route::post('simulation', 'LoanController@simulation')->name('simulation');
    Route::get('offers','FrontEndController@offers')->name('offers');
    Route::get('insurance','FrontEndController@insurance')->name('insurance');
    Route::get('how-it-work','FrontEndController@howItWork')->name('how.it.work');
    Route::get('request','FrontEndController@request')->name('request');
    Route::post('request','LoanController@requestPost')->name('request.post');
    Route::get('simulation','FrontEndController@simulation')->name('simulation');
    Route::get('faq','FrontEndController@faq')->name('faq');
    Route::get('contact','FrontEndController@contact')->name('contact');
    Route::post('contact','ContactController@contactPost')->name('contact.post');
    Route::get('about','FrontEndController@about')->name('about');
    Route::get('mobility','FrontEndController@mobility')->name('mobility');
    Route::get('card','FrontEndController@card')->name('card');
    Route::get('card/request','FrontEndController@cardRequest')->name('card.request');
    Route::post('card/request','LoanController@cardRequestPost')->name('card.request.post');
});

/*Route::domain('{lang}.'.env('APP_DOMAIN'))->group(function () {
    Route::get('home', 'FrontEndController@welcome')->name('welcome');
    Route::post('simulation', 'LoanController@simulation')->name('simulation');
    Route::get('offers','FrontEndController@offers')->name('offers');
    Route::get('insurance','FrontEndController@insurance')->name('insurance');
    Route::get('how-it-work','FrontEndController@howItWork')->name('how.it.work');
    Route::get('request','FrontEndController@request')->name('request');
    Route::post('request','LoanController@requestPost')->name('request.post');
    Route::get('simulation','FrontEndController@simulation')->name('simulation');
    Route::get('faq','FrontEndController@faq')->name('faq');
    Route::get('contact','FrontEndController@contact')->name('contact');
    Route::post('contact','ContactController@contactPost')->name('contact.post');
    Route::get('about','FrontEndController@about')->name('about');
});*/
