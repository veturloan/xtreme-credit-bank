<!-- Top Bar -->
<div class="u-header__section g-brd-bottom g-brd-gray-light-v4 g-transition-0_3">
    <div class="container">
        <div class="row justify-content-between align-items-center g-mx-0--lg">
            <div class="col-12 col-sm-auto order-sm-2 g-py-5 g-py-20--sm text-center">
                <!-- Logo -->
                <a class="navbar-brand" href="{{ route('welcome',app()->getLocale()) }}">
                    <img src="{{ asset('images/logo/logo.png') }}" alt="{{ env('APP_NAME') }}" height="41">
                </a>
                <!-- End Logo -->
            </div>

            <div class="col-6 col-sm-auto order-sm-1 g-pl-0--sm g-py-5 g-py-20--sm">
                <ul class="list-inline g-pt-1 mb-0">
                    <!-- Telephone -->
                    <li class="list-inline-item g-pos-rel g-mr-10">
                        <a href="tel:{{ env('SITE_PHONE') }}" class="g-color-text g-color-primary--hover g-font-weight-400 g-text-underline--none--hover">
                            <i class="icon-communication-163 u-line-icon-pro g-font-size-18 g-valign-middle g-color-black g-mr-5 g-mt-minus-2"></i>
                            {{ env('SITE_PHONE') }}
                        </a>
                    </li>
                    <!-- Language -->
                    <li class="list-inline-item g-pos-rel g-mx-4">
                        <a id="languages-dropdown-invoker-2" class="g-color-text g-color-primary--hover g-font-weight-400 g-text-underline--none--hover" href="#"
                           aria-controls="languages-dropdown-2"
                           aria-haspopup="true"
                           aria-expanded="false"
                           data-dropdown-target="#languages-dropdown-2"
                           data-dropdown-type="css-animation"
                           data-dropdown-duration="300"
                           data-dropdown-hide-on-scroll="false"
                           data-dropdown-animation-in="fadeIn"
                           data-dropdown-animation-out="fadeOut">
                            <img src="{{ config('lang_list.flags.'.app()->getLocale()) }}" alt="{{ config('lang_list.languages.'.app()->getLocale()) }}" height="11" class="mr-1">
                            {{ config('lang_list.languages.'.app()->getLocale()) }}
                        </a>
                        <ul id="languages-dropdown-2" class="list-unstyled u-shadow-v29 g-pos-abs g-left-0 g-bg-white g-width-160 g-pb-5 g-mt-19 g-z-index-4"
                            aria-labelledby="languages-dropdown-invoker-2">
                            @foreach(config('lang_list.languages') as $k => $v)
                                <li>
                                    <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ route(Route::currentRouteName(),$k) }}">
                                        <img src="{{ config('lang_list.flags')[$k] }}" alt="{{ $v }}" height="11" class="mr-1">
                                        {{ $v }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <!-- End Language -->
                </ul>
            </div>

            <div class="col-6 col-sm-auto order-sm-3 g-py-5 g-pr-0 g-py-20--sm">
                <!-- List -->
                <ul class="list-inline g-overflow-hidden g-pt-1 g-mx-minus-4 mb-0">
                    <li class="list-inline-item g-mx-4">
                        <a class="text-white g-font-weight-400 g-text-underline--none--hover text-uppercase btn btn-sm" href="{{ env('BANK_SOFT_URL').'/register' }}" target="_blank" style="border-radius: 0!important;background-color: #FB8500">
                            {{ __('Opening an Account') }}
                        </a>
                    </li>
                    <li class="list-inline-item g-color-gray-light-v3 g-mx-4">|</li>
                    <li class="list-inline-item g-mx-4">
                        <a class="btn btn-sm g-bg-darkblue g-color-white g-font-weight-400 g-text-underline--none--hover text-uppercase" href="{{ env('BANK_SOFT_URL') }}" target="_blank" style="border-radius: 0!important;">
                            <i class="fa fa-user-circle mr-2"></i>
                            {{ __('Login') }}
                        </a>
                    </li>
                </ul>
                <!-- End List -->
            </div>
        </div>
    </div>
</div>
<!-- End Top Bar -->
