<!-- Footer -->
<footer>
    <div class="g-bg-teal g-color-white" style="background-color: #219EBC !important;">
        <div class="container g-pt-70 g-pb-40">
            <div class="row">
                <div class="col-6 col-md-3 g-mb-30">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Links') }}</h2>
                    </div>

                    <!-- Links -->
                    <ul class="list-unstyled mb-0">
                        <li>
                            <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('about',app()->getLocale()) }}">
                                {{ __('How we are?') }}
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('offers',app()->getLocale()) }}">
                                {{ __('Our credit offers') }}
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('insurance',app()->getLocale()) }}">
                                {{ __('Insurance') }}
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('how.it.work',app()->getLocale()) }}">
                                {{ __('How it work ?') }}
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('faq',app()->getLocale()) }}">
                                {{ __('FAQ') }}
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                        <li>
                            <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('contact',app()->getLocale()) }}">
                                {{ __('Contact Us') }}
                                <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                            </a>
                        </li>
                    </ul>
                    <!-- End Links -->
                </div>

                <div class="col-6 col-md-6 g-mb-30">

                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Credit offers') }}</h2>
                    </div>

                   <div class="row">
                           <div class="col-sm-4">
                           <ul class="list-unstyled mb-0">
                               @foreach(config('loan_types.list') as $k => $v)
                                   @if(in_array($k,config('loan_types.column_1')))
                                       <li>
                                           <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('offers',app()->getLocale()) }}">
                                               <span>{{ __($k) }}</span>
                                               <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                           </a>
                                       </li>
                                   @endif
                               @endforeach
                           </ul>
                       </div>

                       <div class="col-sm-4">
                           <ul class="list-unstyled mb-0">
                               @foreach(config('loan_types.list') as $k => $v)
                                   @if(in_array($k,config('loan_types.column_2')))
                                       <li>
                                           <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('offers',app()->getLocale()) }}">
                                               <span>{{ __($k) }}</span>
                                               <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                           </a>
                                       </li>
                                   @endif
                               @endforeach
                           </ul>
                       </div>

                       <div class="col-sm-4">
                           <ul class="list-unstyled mb-0">
                               @foreach(config('loan_types.list') as $k => $v)
                                   @if(in_array($k,config('loan_types.column_3')))
                                       <li>
                                           <a class="u-link-v6 g-color-white g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="{{ route('offers',app()->getLocale()) }}">
                                               <span>{{ __($k) }}</span>
                                               <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                                           </a>
                                       </li>
                                   @endif
                               @endforeach
                           </ul>
                       </div>
                   </div>

                </div>

                <div class="col-6 col-md-3 g-mb-30">
                    <!-- Links -->
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Newsletter') }}</h2>
                    </div>

                    <p class="g-color-white">{{ __('Subscribe to our newsletter and stay up to date with the latest news and deals!') }}</p>

                    <div class="input-group border-0 g-mb-30">
                        <input class="form-control border-0 rounded-0 g-px-12 g-py-8" type="email" placeholder="{{ __('Email address') }}">
                        <div class="input-group-addon g-brd-around-none p-0">
                            <button class="btn u-btn-red rounded-0 g-px-12 g-py-8" type="submit" role="button"><i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                    <!-- End Links -->

                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Address') }}</h2>
                    </div>
                    <address class="g-font-size-13 g-color-white g-font-weight-600 g-line-height-2 mb-0">
                        Richtstrasse 16,<br>
                        7007 Chur,<br>
                        Coire, Suisse
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div style="background-color: #FB8500">
        <div class="container g-pt-30">
            <div class="row align-items-center">
                <div class="col-md-4 text-center text-md-left g-mb-30">
                    <!-- Logo -->
                    <a class="g-text-underline--none--hover mr-4" href="{{ route('welcome',app()->getLocale()) }}">
                        <img class="g-width-180" src="{{ url('images/logo/logo-light.png') }}" alt="XTREME CREDIT BANK">
                    </a>
                    <!-- End Logo -->
                    <p class="d-inline-block align-middle g-font-size-13 mb-0 g-color-white">&copy; 2008 {{ env('APP_NAME') }}.<br>All Rights Reserved.</p>
                </div>

                <div class="col-md-4 g-mb-30">
                    <!-- Social Icons -->
                    <ul class="list-inline text-center mb-0">
                        <li class="list-inline-item">
                            <a class="u-icon-v3 u-icon-size--sm g-color-white g-color-primary--hover g-bg-transparent g-bg-main--hover rounded" href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a class="u-icon-v3 u-icon-size--sm g-color-white g-color-primary--hover g-bg-transparent g-bg-main--hover rounded" href="#"><i class="fa fa-youtube"></i></a>
                        </li>
                    </ul>
                    <!-- End Social Icons -->
                </div>

                <div class="col-md-4 g-mb-30 text-right">
                    <div class="d-inline-block g-mx-15">
                        <h4 class="g-color-white g-font-size-11 text-left text-uppercase">{{ __('Email') }}</h4>
                        <a href="#" class="g-color-white">{{ env('SITE_CONTACT_EMAIL') }}</a>
                    </div>
                    <div class="d-inline-block g-mx-15">
                        <h4 class="g-color-white g-font-size-11 text-left text-uppercase">{{ __('Phone') }}</h4>
                        <a href="#">{{ env('SITE_PHONE') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Go to Top -->
<a class="js-go-to u-go-to-v1 g-width-40 g-height-40 g-color-primary g-bg-main-opacity-0_5 g-bg-main--hover g-bg-main--focus g-font-size-12 rounded" href="#" data-type="fixed" data-position='{
       "bottom": 15,
       "right": 15
     }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
    <i class="hs-icon hs-icon-arrow-top"></i>
</a>
<!-- End Go to Top -->

<div id="fixed-social">
    <div>
        <a href="https://wa.me/{{ env('SITE_WHATSAPP') }}" class="fixed-gplus" target="_blank"><i class="fa fa-whatsapp"></i> <span>Whatsapp</span></a>
    </div>
    <div>
        <a href="tel:{{ env('SITE_PHONE') }}" class="fixed-linkedin" target="_blank"><i class="fa fa-phone"></i> <span>{{ env('SITE_PHONE') }}</span></a>
    </div>
</div>
