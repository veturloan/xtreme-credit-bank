<section style="background-color: #FB8500">
    <div class="container g-py-100">
        <!-- Icon Blocks -->
        <div class="row">
            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-education-087 u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Insurance') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __('XTREME CREDIT BANK supports you on a daily basis, because it is crucial for your needs. Protect yourself in the event of accidental death or disability.') }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-education-035 u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Funding') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __('To put your loan application online, you must meet certain conditions, such as: Being a capable adult. You should also have regular cash flow and ...') }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>
        </div>
        <!-- End Icon Blocks -->

        <!-- Icon Blocks -->
        <div class="row">
            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-education-141 u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Credit property') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __('You can really multiply and compare simulations to find the right compromise in terms of interest rate, loan duration, monthly payments and personal contribution ...') }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-finance-256 u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Credit repayment') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __("However, sometimes due to a stroke (loss of spouse's work, illness, accident ...), the critical rate of the debt ratio has been exceeded and the rest of life is ultimately too low for .. .") }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>
        </div>
        <!-- End Icon Blocks -->

        <!-- Icon Blocks -->
        <div class="row">
            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-finance-260 u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Our interest rate') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __('Our interest rates vary depending on the amount desired and the billing period. Our prices are relatively low and vary between 2% and 8.90%. Select an employee ...') }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-finance-009 u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Credit') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __("Assets affected by inalienability and not placed on the market under article 2397 of the civil code are not subject to a conventional mortgage. It's so consistent ...") }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>
        </div>
        <!-- End Icon Blocks -->

        <!-- Icon Blocks -->
        <div class="row">
            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-user-following u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Online register') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __('To put your loan application online, you must meet certain conditions, such as: B. Being a capable adult. In addition, you must have a regular cash flow and be able to pay ...') }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>

            <div class="col-lg-6 g-mb-30">
                <!-- Icon Blocks -->
                <div class="u-shadow-v19 g-bg-white rounded g-pa-30">
                    <div class="media g-mb-15">
                        <div class="d-flex mr-4">
                  <span class="g-color-gray-dark-v4 g-font-size-26">
                    <i class="icon-finance-150 u-line-icon-pro"></i>
                  </span>
                        </div>
                        <div class="media-body">
                            <h3 class="h5 g-color-black mb-20">{{ __('Mutual insurance simulation') }}</h3>
                            <div class="g-width-30 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
                            <p class="g-color-gray-dark-v4 g-mb-0">
                                {{ __('It is indeed possible to multiply and compare simulations to find the right compromise on interest rates, loan duration, monthly payments and personal contribution, and make the most of it.') }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Icon Blocks -->
            </div>
        </div>
        <!-- End Icon Blocks -->
    </div>
</section>
