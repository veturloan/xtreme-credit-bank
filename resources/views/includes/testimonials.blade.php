<!-- Call To Action -->
<section id="go-to-content" class="g-pt-100 g-pb-100 g-bg-secondary">
    <div class="container g-pos-rel g-pt-100">
        <div class="row justify-content-end align-items-center">
            <div class="col-sm-6 order-sm-2 g-mb-30">
                <!-- Testimonials - Carousel -->
                <div id="carouselCus19-1" class="js-carousel" data-infinite="true" data-autoplay="true" data-speed="10000" data-nav-for="#carouselCus19-2" data-in-effect="slideInRight" data-out-effect="slideInRight">
                    <div class="js-slide g-pl-50--lg">
                        <!-- Testimonials - Content -->
                        <div class="media mb-3">
                            <div class="d-flex mr-3">
                                <span class="g-font-secondary g-font-size-40 g-opacity-0_3 g-pos-rel g-top-minus-10">“</span>
                            </div>
                            <div class="media-body">
                                <blockquote class="g-brd-left-none g-font-style-italic g-font-size-20 g-pl-0">
                                    {{ __('Thank you to the XTREME CREDIT BANK team. Thank you for your sincerity and speed. I received my credit of 35.000 euros on my bank account (Agram banka) without any complications. With this credit, I can finance my activities well. You are a 5 star bank. God bless you.') }}
                                    <span class="align-self-end g-font-secondary g-font-size-40 g-opacity-0_3 g-line-height-0 align-bottom g-pos-rel g-top-minus-10">”</span>
                                </blockquote>
                            </div>
                        </div>

                        <div class="g-pl-30">
                            <h3 class="h4 mb-0">Gordan B.</h3>
                            <span class="d-block g-font-size-18 g-pl-20">— {{ __('Croatia') }}</span>
                        </div>
                        <!-- End Testimonials - Content -->
                    </div>
                    <div class="js-slide g-pl-50--lg">
                        <!-- Testimonials - Content -->
                        <div class="media mb-3">
                            <div class="d-flex mr-3">
                                <span class="g-font-secondary g-font-size-40 g-opacity-0_3 g-pos-rel g-top-minus-10">“</span>
                            </div>
                            <div class="media-body">
                                <blockquote class="g-brd-left-none g-font-style-italic g-font-size-20 g-pl-0">
                                    {{ __('XTREME CREDIT BANK, a bank of trust and discretion. Thank you for financing my project. I have received my credit of 80.000 LEVS in my account at DSK bank. If my family has a smile on their face, it`\'s thanks to you.') }}
                                    <span class="align-self-end g-font-secondary g-font-size-40 g-opacity-0_3 g-line-height-0 align-bottom g-pos-rel g-top-minus-10">”</span>
                                </blockquote>
                            </div>
                        </div>

                        <div class="g-pl-30">
                            <h3 class="h4 mb-0">Anastasova V.</h3>
                            <span class="d-block g-font-size-18 g-pl-20">— {{ __('Bulgaria') }}</span>
                        </div>
                        <!-- End Testimonials - Content -->
                    </div>
                    <div class="js-slide g-pl-50--lg">
                        <!-- Testimonials - Content -->
                        <div class="media mb-3">
                            <div class="d-flex mr-3">
                                <span class="g-font-secondary g-font-size-40 g-opacity-0_3 g-pos-rel g-top-minus-10">“</span>
                            </div>
                            <div class="media-body">
                                <blockquote class="g-brd-left-none g-font-style-italic g-font-size-20 g-pl-0">
                                    {{ __('I thank you for the help and simplicity you give to my company. With your XTREME CREDIT BANK INSURANCE service, my employees and workers are now safe. I thank you for the fast assistance you give us.') }}
                                    <span class="align-self-end g-font-secondary g-font-size-40 g-opacity-0_3 g-line-height-0 align-bottom g-pos-rel g-top-minus-10">”</span>
                                </blockquote>
                            </div>
                        </div>

                        <div class="g-pl-30">
                            <h3 class="h4 mb-0">Carlos L.</h3>
                            <span class="d-block g-font-size-18 g-pl-20">— {{ __('Equator') }}</span>
                        </div>
                        <!-- End Testimonials - Content -->
                    </div>
                    <div class="js-slide g-pl-50--lg">
                        <!-- Testimonials - Content -->
                        <div class="media mb-3">
                            <div class="d-flex mr-3">
                                <span class="g-font-secondary g-font-size-40 g-opacity-0_3 g-pos-rel g-top-minus-10">“</span>
                            </div>
                            <div class="media-body">
                                <blockquote class="g-brd-left-none g-font-style-italic g-font-size-20 g-pl-0">
                                    {{ __('I finally received my VISA card by DHL. Your VISA card is easy to use. With your card, transactions are easy. Your cards are available at all counters in Latvia.') }}
                                    <span class="align-self-end g-font-secondary g-font-size-40 g-opacity-0_3 g-line-height-0 align-bottom g-pos-rel g-top-minus-10">”</span>
                                </blockquote>
                            </div>
                        </div>

                        <div class="g-pl-30">
                            <h3 class="h4 mb-0">Liana G.</h3>
                            <span class="d-block g-font-size-18 g-pl-20">— {{ __('Latvia') }}</span>
                        </div>
                        <!-- End Testimonials - Content -->
                    </div>
                    <div class="js-slide g-pl-50--lg">
                        <!-- Testimonials - Content -->
                        <div class="media mb-3">
                            <div class="d-flex mr-3">
                                <span class="g-font-secondary g-font-size-40 g-opacity-0_3 g-pos-rel g-top-minus-10">“</span>
                            </div>
                            <div class="media-body">
                                <blockquote class="g-brd-left-none g-font-style-italic g-font-size-20 g-pl-0">
                                    {{ __('I am very happy to have received the 75,000 euros in my account. With your credit, my husband will be cured of his illness and I will be able to start my own business. You are a great bank.') }}
                                    <span class="align-self-end g-font-secondary g-font-size-40 g-opacity-0_3 g-line-height-0 align-bottom g-pos-rel g-top-minus-10">”</span>
                                </blockquote>
                            </div>
                        </div>

                        <div class="g-pl-30">
                            <h3 class="h4 mb-0">Vitaly G.</h3>
                            <span class="d-block g-font-size-18 g-pl-20">— {{ __('Singapore') }}</span>
                        </div>
                        <!-- End Testimonials - Content -->
                    </div>
                    <div class="js-slide g-pl-50--lg">
                        <!-- Testimonials - Content -->
                        <div class="media mb-3">
                            <div class="d-flex mr-3">
                                <span class="g-font-secondary g-font-size-40 g-opacity-0_3 g-pos-rel g-top-minus-10">“</span>
                            </div>
                            <div class="media-body">
                                <blockquote class="g-brd-left-none g-font-style-italic g-font-size-20 g-pl-0">
                                    {{ __('Thank you for your patience and for your explanations on the assembly of our file. Very good follow-up and good reactivity at delicate moments. We\'re back on good feet now thanks to you. Cordially') }}
                                    <span class="align-self-end g-font-secondary g-font-size-40 g-opacity-0_3 g-line-height-0 align-bottom g-pos-rel g-top-minus-10">”</span>
                                </blockquote>
                            </div>
                        </div>

                        <div class="g-pl-30">
                            <h3 class="h4 mb-0">Weiche D.</h3>
                            <span class="d-block g-font-size-18 g-pl-20">— {{ __('Germany') }}</span>
                        </div>
                        <!-- End Testimonials - Content -->
                    </div>
                </div>
                <!-- End Testimonials - Carousel -->
            </div>

            <div class="col-sm-6 order-sm-1 g-pos-stc g-mb-30">
                <!-- Carousel - Image -->
                <div id="carouselCus19-2" class="js-carousel g-pos-stc" data-infinite="true" data-nav-for="#carouselCus19-1" data-arrows-classes="u-icon-v3 u-icon-size--sm g-absolute-centered--y g-color-primary g-color-white--hover g-bg-primary-opacity-0_1 g-bg-primary--hover rounded-circle g-pa-11" data-arrow-left-classes="fa fa-angle-left g-left-0" data-arrow-right-classes="fa fa-angle-right g-right-0">
                    <div class="js-slide g-min-height-50vh g-bg-size-cover g-bg-pos-top-center" data-bg-img-src="{{ url('images/testimonials/1.jpg') }}"></div>
                    <div class="js-slide g-min-height-50vh g-bg-size-cover g-bg-pos-top-center" data-bg-img-src="{{ url('images/testimonials/2.jpg') }}"></div>
                    <div class="js-slide g-min-height-50vh g-bg-size-cover g-bg-pos-top-center" data-bg-img-src="{{ url('images/testimonials/3.jpg') }}"></div>
                    <div class="js-slide g-min-height-50vh g-bg-size-cover g-bg-pos-top-center" data-bg-img-src="{{ url('images/testimonials/4.jpg') }}"></div>
                    <div class="js-slide g-min-height-50vh g-bg-size-cover g-bg-pos-top-center" data-bg-img-src="{{ url('images/testimonials/5.jpg') }}"></div>
                    <div class="js-slide g-min-height-50vh g-bg-size-cover g-bg-pos-top-center" data-bg-img-src="{{ url('images/testimonials/6.jpg') }}"></div>
                </div>
                <!-- End Carousel - Image -->
            </div>
        </div>
    </div>

</section>
<!-- End Call To Action -->
