<!-- Featured Offers -->
<div class="container g-py-100">
    <div class="row">
        @foreach(config('loan_types.list') as $loan_type => $loan_type_resume)
            @if(in_array($loan_type,config('loan_types.featured')))
                <div class="col-md-6 col-lg-4 g-mb-30">
                    <!-- Services -->
                    <article class="u-block-hover u-shadow-v29--hover g-bg-main--hover rounded g-pa-35">
                        <div class="d-flex align-items-center g-brd-bottom g-brd-2 g-brd-primary g-pb-20 g-mb-20">
                <span class="align-middle u-icon-v3 u-icon-size--xs g-color-white g-bg-red rounded-circle mr-2">
                  <i class="icon-education-200 u-line-icon-pro"></i>
                </span>
                            <h3 class="d-inline-block g-color-red g-color-white--hover g-font-weight-600 g-transition-0_3 mb-0">{{ __($loan_type) }}</h3>
                        </div>

                        <div class="mb-4">
                            <p class="g-color-white--hover g-transition-0_3">{{ __($loan_type_resume) }}</p>
                        </div>

                        <span class="u-link-v6 g-color-primary g-color-primary--hover g-font-size-12 text-uppercase g-text-underline--none--hover g-transition-0_3">
                    {{--{{ __('Read More') }}--}}
                    <span class="u-link-v6-arrow g-font-size-18">&rightarrow;</span>
                  </span>

                        <a class="u-link-v2" href="#"></a>
                    </article>
                    <!-- End Featured Offers -->
                </div>
            @endif
        @endforeach
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="text-right">
                <a class="u-link-v7 g-color-main g-color-primary--hover g-font-size-13 g-text-underline--none--hover" href="{{ route('offers',app()->getLocale()) }}">
                    {{ __('All credit offers') }}
                    <span class="u-link-v7-arrow g-font-size-18">→</span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Services -->
