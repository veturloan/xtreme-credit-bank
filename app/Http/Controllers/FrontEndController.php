<?php

namespace App\Http\Controllers;

use App\Forms\CardForm;
use App\Forms\ContactForm;
use App\Forms\LoanRequestForm;
use App\Forms\SimulationForm;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class FrontEndController extends Controller
{
    use FormBuilderTrait;



    public function welcome()
    {
        $form = $this->form(SimulationForm::class, [
            'method' => 'POST',
            'route' => ['simulation','lang' => app()->getLocale()],
        ]);
        return view('welcome',compact('form'));
    }

    public function request()
    {
        $form = $this->form(LoanRequestForm::class, [
            'method' => 'POST',
            'route' => ['request.post','lang' => app()->getLocale()],
        ]);
        return view('request',compact('form'));
    }

    public function cardRequest()
    {
        $form = $this->form(CardForm::class, [
            'method' => 'POST',
            'route' => ['card.request.post','lang' => app()->getLocale()],
        ]);
        return view('card-request',compact('form'));
    }

    public function contact()
    {
        $form = $this->form(ContactForm::class, [
            'method' => 'POST',
            'route' => ['contact.post', 'lang' => app()->getLocale()]
        ]);
        return view('contact',compact('form'));
    }

    public function insurance()
    {
        return view('insurance');
    }

    public function howItWork()
    {
        return view('how-it-work');
    }

    public function simulation()
    {
        $form = $this->form(SimulationForm::class, [
            'method' => 'POST',
            'route' => ['simulation','lang' => app()->getLocale()],
        ]);
        return view('simulation',compact('form'));
    }

    public function faq()
    {
        return view('faq');
    }

    public function about()
    {
        return view('about');
    }

    public function offers()
    {
        return view('offers');
    }

    public function mobility()
    {
        return view('mobility');
    }

    public function card()
    {
        return view('card');
    }
}
