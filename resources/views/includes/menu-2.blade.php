<div class="u-header__section u-header__section--light g-color-white g-transition-0_3" style="background-color: #FB8500!important;">
    <nav class="js-mega-menu navbar navbar-expand-lg">
        <div class="container">
            <!-- Responsive Toggle Button -->
            <button type="button" class="navbar-toggler navbar-toggler-right btn ml-auto g-line-height-1 g-brd-none g-pa-0"
                    aria-label="Toggle navigation"
                    aria-expanded="false"
                    aria-controls="navBar"
                    data-toggle="collapse"
                    data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Navigation -->
            <div id="navBar" class="collapse navbar-collapse align-items-center flex-sm-row g-color-white">
                <ul class="navbar-nav mx-auto g-font-weight-600 g-color-white">
                    <!-- Home - Submenu -->
                    <li class="nav-item g-mx-10--lg g-mx-15--xl">
                        <a href="{{ route('welcome',app()->getLocale()) }}" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20">{{ __('home') }}</a>
                    </li>
                    <li class="nav-item hs-has-sub-menu g-mx-10--lg g-mx-15--xl">
                        <a id="nav-link--home" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20" href="#"
                           aria-haspopup="true"
                           aria-expanded="false"
                           aria-controls="nav-submenu--home">
                            {{ __('banking services') }}
                        </a>

                        <!-- Submenu -->
                        <ul id="nav-submenu--home" class="hs-sub-menu list-unstyled u-shadow-v11 g-min-width-220 g-brd-top g-brd-primary g-brd-top-2 g-mt-7 text-uppercase"
                            aria-labelledby="nav-link--home">
                            <li class="dropdown-item">
                                <a class="nav-link g-color-gray-dark-v4" href="{{ route('mobility',app()->getLocale()) }}">
                                    {{ __('banking mobility') }}
                                </a>
                            </li>
                            <li class="dropdown-item">
                                <a class="nav-link g-color-gray-dark-v4" href="{{ route('card',app()->getLocale()) }}">
                                    {{ __('credit card') }}
                                </a>
                            </li>
                            <li class="dropdown-item">
                                <a class="nav-link g-color-gray-dark-v4" href="{{ route('insurance',app()->getLocale()) }}">
                                    {{ __('Insurance') }}
                                </a>
                            </li>
                        </ul>
                        <!-- End Submenu -->
                    </li>
                    <!-- End Home - Submenu -->

                    <li class="nav-item g-mx-10--lg g-mx-15--xl">
                        <a href="{{ route('offers',app()->getLocale()) }}" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20">{{ __('Credit offers') }}</a>
                    </li>

                    <li class="nav-item g-mx-10--lg g-mx-15--xl">
                        <a href="{{ route('request',app()->getLocale()) }}" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20">{{ __('Make a Request') }}</a>
                    </li>

                    <li class="nav-item g-mx-10--lg g-mx-15--xl">
                        <a href="{{ route('faq',app()->getLocale()) }}" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20">FAQ</a>
                    </li>

                    <li class="nav-item g-mx-10--lg g-mx-15--xl">
                        <a href="{{ route('contact',app()->getLocale()) }}" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20">{{ __('Contact Us') }}</a>
                    </li>

                </ul>
            </div>
            <!-- End Navigation -->
        </div>
    </nav>
</div>
