<!-- MasterSlider Main -->
<div class="ms-parent">
    <div id="masterslider" class="master-slider ms-skin-minimal">
        <div class="ms-slide" data-delay="6" data-fill-mode="fill">
            <img src="{{ url('assets/img/blank.gif') }}" alt="" title="" data-src="{{ asset('images/sliders/slide-bg/home-2.jpg') }}" />
            <a href="{{ env('BANK_SOFT_URL').'/register' }}"
               target="_self"
               class="ms-layer msp-cn-69-12 ms-btn ms-btn-box ms-btn-m msp-preset-btn-169 account-btn"
               data-effect="t(true,n,n,-100,n,n,n,n,n,n,n,n,n,n,n)"
               data-duration="2400"
               data-delay="2600"
               data-ease="easeOutQuint"
               data-type="button"
               data-offset-x="212"
               data-offset-y="370"
               data-origin="tl"
               data-position="normal">{{ __('My Account') }}</a>
            <div class="ms-layer msp-cn-69-11"
                 style="color: #003049;font-weight: 400"
                 data-effect="t(true,n,n,10,n,n,n,n,0.8,0.8,n,n,n,n,n)"
                 data-duration="2425"
                 data-delay="1737"
                 data-ease="easeOutQuint"
                 data-offset-x="212"
                 data-offset-y="303"
                 data-origin="tl"
                 data-position="normal">{{ __('Excellence at your service.') }}
            </div>
            <div class="ms-layer msp-cn-69-9"
                 style="color: #003049"
                 data-effect="t(true,n,30,10,n,n,n,n,n,n,n,n,n,n,n)"
                 data-duration="2425"
                 data-delay="1150"
                 data-ease="easeOutQuint"
                 data-offset-x="212"
                 data-offset-y="244"
                 data-origin="tl"
                 data-position="normal"
                 data-masked="true">{{ __('Your Bank') }}
            </div>
            <div class="ms-layer msp-cn-69-8"
                 style="color: #003049;font-weight: 700;"
                 data-effect="t(true,n,-30,10,n,n,n,n,n,n,n,n,n,n,n)"
                 data-duration="2400"
                 data-delay="900"
                 data-ease="easeOutQuint"
                 data-offset-x="212"
                 data-offset-y="175"
                 data-origin="tl"
                 data-position="normal"
                 data-masked="true">XTREME CREDIT BANK
            </div>
        </div>
        <div class="ms-slide" data-delay="6" data-fill-mode="fill">
            <img src="{{ url('assets/img/blank.gif') }}" alt="" title="" data-src="{{ asset('images/sliders/slide-bg/home-1.jpg') }}" />
            <a href="{{ env('BANK_SOFT_URL') }}"
               target="_self"
               class="ms-layer login-btn msp-cn-69-18 ms-btn ms-btn-box ms-btn-m msp-preset-btn-170"
               data-effect="t(true,n,150,10,n,n,n,n,n,n,n,n,n,n,n)"
               data-duration="2512"
               data-delay="1800"
               data-ease="easeOutQuint"
               data-type="button"
               data-offset-x="212"
               data-offset-y="453"
               data-origin="tl"
               data-position="normal">{{ __('Login') }}</a>
            <div class="ms-layer msp-cn-69-16"
                 style="color: #ffffff;font-weight: 400"
                 data-effect="t(true,n,300,10,n,n,n,n,n,n,n,n,n,n,n)"
                 data-duration="2512"
                 data-delay="600"
                 data-ease="easeOutQuint"
                 data-offset-x="212"
                 data-offset-y="374"
                 data-origin="tl"
                 data-position="normal"
                 data-masked="true">
                {{ __('Your money is following you everywhere and safely.') }}
            </div>
            <div class="ms-layer msp-preset-7"
                 style="color: #ffffff"
                 data-effect="t(true,n,250,10,n,n,n,n,n,n,n,n,n,n,n)"
                 data-duration="2487"
                 data-delay="300"
                 data-ease="easeOutQuint"
                 data-offset-x="212"
                 data-offset-y="297"
                 data-origin="tl"
                 data-position="normal"
                 data-masked="true">{{ __('Banking Mobility') }}
            </div>
            <div class="ms-layer msp-cn-69-13"
                 style="color: #fff9e5"
                 data-effect="t(true,n,200,10,n,n,n,n,n,n,n,n,n,n,n)"
                 data-duration="2500"
                 data-ease="easeOutQuint"
                 data-offset-x="212"
                 data-offset-y="226"
                 data-origin="tl"
                 data-position="normal"
                 data-masked="true">ONYX Mobile
            </div>
        </div>
    </div>
</div>
<!-- END MasterSlider Main -->
