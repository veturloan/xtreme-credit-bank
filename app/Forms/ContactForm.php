<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ContactForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('last_name','text',[
                'label' => __('Last Name'),
                'attr' => [
                    'placeholder' => __('Your last name...'),
                    'class' => 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-6 form-group g-mb-20'
                ]
            ])
            ->add('first_name','text',[
                'label' => __('First Name'),
                'attr' => [
                    'placeholder' => __('Your first name...'),
                    'class' => 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-6 form-group g-mb-20'
                ]
            ])
            ->add('email','email',[
                'label' => __('E-mail Address'),
                'attr' => [
                    'placeholder' => __('Your e-mail address...'),
                    'class' => 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'email',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-6 form-group g-mb-20'
                ]
            ])
            ->add('phone','text',[
                'label' => __('Telephone'),
                'attr' => [
                    'placeholder' => 'Your phone number...',
                    'class' => 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'numeric',
                    'digits_between:7,250'
                ],
                'wrapper' => [
                    'class' => 'col-md-6 form-group g-mb-20'
                ]
            ])
            ->add('subject','text',[
                'label' => __('Subject'),
                'attr' => [
                    'placeholder' => __('The subject of your message...'),
                    'class' => 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250',
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-20'
                ]
            ])
            ->add('message','textarea',[
                'label' => __('Message'),
                'attr' => [
                    'placeholder' => __('Your message content here...'),
                    'class' => 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover g-resize-none rounded-3 g-py-13 g-px-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:1000',
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-40'
                ]
            ])
            ->add(__('Send'),'submit',[
                'attr' => [
                    'class' => 'btn u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-rounded-25 g-py-15 g-px-30',
                ],
                'wrapper' => [
                    'class' => 'text-center'
                ]
            ]);
    }
}
