@extends('layouts.frontend-2')

@section('title')

    {{ __('FAQ') }}

@endsection

@section('content')

    <!-- Contact Form -->
    <section class="container g-mt-100">
        <div class="container">
            <section class="text-center g-py-100">
                <h4 class="h1 g-color-black g-font-weight-600">{{ __('Questions answered') }}</h4>
            </section>
            <img class="img-fluid w-100" src="{{ url('images/faq.jpg') }}" alt="Image Description">
        </div>

        <!-- Accordion -->
        <section class="g-bg-gray-gradient-opacity-v1">
            <div class="container g-py-100">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <!-- Heading -->
                        <div class="text-center g-mb-60">
                            <div class="d-inline-block g-width-35 g-height-1 g-bg-primary mb-2"></div>
                            <h2 class="g-color-black g-font-weight-600 mb-2">{{ __('Frequently Asked Questions') }}</h2>
                            <p class="lead g-width-60x--md mx-auto">{{ __('We aim high at being focused on building relationships with our clients and community.') }}</p>
                        </div>
                        <!-- End Heading -->

                        <div id="accordion" class="u-accordion u-accordion-color-primary" role="tablist" aria-multiselectable="true">
                            <!-- Card -->
                            <div class="card g-brd-none rounded g-mb-20">
                                <div id="accordion-heading-01" class="g-pa-0" role="tab">
                                    <h5 class="mb-0">
                                        <a class="collapsed d-flex justify-content-between u-shadow-v19 g-color-main g-text-underline--none--hover rounded g-px-30 g-py-20" href="#accordion-body-01" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-body-01">
                                            {{ __('What are the steps involved in applying for funding online?') }}
                                            <span class="u-accordion__control-icon g-color-primary">
                          <i class="fa fa-angle-down"></i>
                          <i class="fa fa-angle-up"></i>
                        </span>
                                        </a>
                                    </h5>
                                </div>
                                <div id="accordion-body-01" class="collapse" role="tabpanel" aria-labelledby="accordion-heading-01" data-parent="#accordion">
                                    <div class="u-accordion__body g-color-gray-dark-v4 g-pa-30">
                                        <p>{{ __('With XTREME CREDIT BANK, you can easily apply for a loan without having to travel! Here are the 5 steps to follow:') }}</p>
                                        <ol>
                                            <li>{{ __('Depending on your project, you choose the loan that suits you. Thanks to our simulator, depending on the amount you need, you can customize the monthly payment and duration that you think is best suited to your situation. All you have to do is enter your request!') }}</li>
                                            <li>{{ __('You will receive an immediate response in principle, as well as an e-mail summarizing your request.') }}</li>
                                            <li>{{ __('A local loan officer will contact you to process your file. He or she will be responsible for guiding you through the process.') }}</li>
                                            <li>{{ __('You will be asked to provide the necessary information and documents to complete your loan file.') }}</li>
                                            <li>{{ __('After analysis of your request, your credit will be validated, we will ask you for your bank details to make the transfer of funds so that you can use it.') }}</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <!-- End Card -->

                            <!-- Card -->
                            <div class="card g-brd-none rounded g-mb-20">
                                <div id="accordion-heading-02" class="g-pa-0" role="tab">
                                    <h5 class="mb-0">
                                        <a class="collapsed d-flex justify-content-between u-shadow-v19 g-color-main g-text-underline--none--hover rounded g-px-30 g-py-20" href="#accordion-body-02" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-body-02">
                                            {{ __('Can I change the amount of my monthly payments? How long will my credit last?') }}
                                            <span class="u-accordion__control-icon g-color-primary">
                                              <i class="fa fa-angle-down"></i>
                                              <i class="fa fa-angle-up"></i>
                                            </span>
                                        </a>
                                    </h5>
                                </div>
                                <div id="accordion-body-02" class="collapse" role="tabpanel" aria-labelledby="accordion-heading-02" data-parent="#accordion">
                                    <div class="u-accordion__body g-color-gray-dark-v4 g-pa-30">
                                        {{ __("With XTREME CREDIT BANK' loan offers, you can change the amount of your monthly payments at any time, as many times as you want, after studying your file.") }}
                                    </div>
                                </div>
                            </div>
                            <!-- End Card -->

                            <!-- Card -->
                            <div class="card g-brd-none rounded g-mb-20">
                                <div id="accordion-heading-03" class="g-pa-0" role="tab">
                                    <h5 class="mb-0">
                                        <a class="collapsed d-flex justify-content-between u-shadow-v19 g-color-main g-text-underline--none--hover rounded g-px-30 g-py-20" href="#accordion-body-03" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="accordion-body-03">
                                            {{ __("It's been more than 48 hours since I entered my file, and I still don't have an answer?") }}
                                            <span class="u-accordion__control-icon g-color-primary">
                          <i class="fa fa-angle-down"></i>
                          <i class="fa fa-angle-up"></i>
                        </span>
                                        </a>
                                    </h5>
                                </div>
                                <div id="accordion-body-03" class="collapse" role="tabpanel" aria-labelledby="accordion-heading-03" data-parent="#accordion">
                                    <div class="u-accordion__body g-color-gray-dark-v4 g-pa-30">
                                        {{ __('Normally, an e-mail has been sent to you. We therefore invite you to check your mailbox, including the "Spam" folder. If you have not received anything, you can write to a customer advisor at:') }} <strong>{{ env('SITE_CONTACT_EMAIL') }}</strong>
                                    </div>
                                </div>
                            </div>
                            <!-- End Card -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Accordion -->
    </section>

@endsection
