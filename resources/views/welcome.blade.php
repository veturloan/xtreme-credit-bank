@extends('layouts.frontend-2')

@section('content')

    {{--    @include('includes.home-banner')    --}}

    @include('includes.home-slider-2')

{{--    @include('includes.about-section')--}}

    @include('includes.services-2')

    @include('includes.featured-offers')

{{--    @include('includes.team')   --}}

    @include('includes.call-to-action')

    @include('includes.promo-banner')

    @include('includes.testimonials')

    @include('includes.partners')

@endsection

@section('scripts')

    <script >
        $(document).on('ready', function () {
            // initialization of carousel
            $.HSCore.components.HSCarousel.init('.js-carousel');
        });

        $(window).on('load', function() {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');

            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                pageContainer: $('.container'),
                breakpoint: 991
            });

            // initialization of masonry
            $('.masonry-grid').imagesLoaded().then(function () {
                $('.masonry-grid').masonry({
                    columnWidth: '.masonry-grid-sizer',
                    itemSelector: '.masonry-grid-item',
                    percentPosition: true
                });
            });
        });

        var slider = new MasterSlider();

        slider.control('arrows', {
            autohide: true,
            overVideo: true
        });
        slider.setup("masterslider", {
            width: 1366,
            height: 768,
            minHeight: 0,
            space: 0,
            start: 1,
            grabCursor: true,
            swipe: true,
            mouse: true,
            keyboard: true,
            layout: "fullwidth",
            wheel: false,
            autoplay: true,
            instantStartLayers: false,
            loop: true,
            shuffle: false,
            preload: 0,
            heightLimit: true,
            autoHeight: false,
            smoothHeight: true,
            endPause: false,
            overPause: true,
            fillMode: "fill",
            centerControls: false,
            startOnAppear: false,
            layersMode: "center",
            autofillTarget: "",
            hideLayers: false,
            fullscreenMargin: 0,
            speed: 15,
            dir: "h",
            parallaxMode: 'swipe',
            view: "parallaxMask"
        });
    </script>

@stop
