@extends('layouts.frontend-2')

@section('title')

    {{ __('Loan request') }}

@endsection

@section('content')

    <!-- Contact Form -->
    <section class="container g-pt-100 g-mt-100">
        <div class="row g-mb-20">
            <div class="col-lg-12">
                <!-- Heading -->
                <h2 class="h1 g-color-black g-font-weight-700 mb-4 text-center">{{ __('Just fill the request form') }}</h2>
                <!-- End Heading -->
            </div>
        </div>
    </section>

    <section class="g-pb-100">
        <div class="container">
            <div class="row justify-content-center">
                @if(session('success'))
                    <div class="alert alert-success">
                        <h5>{{ session('success') }}</h5>
                    </div>
                @endif
                    {!! form($form) !!}
            </div>
        </div>
    </section>
    <!-- End Contact Form -->

@endsection
