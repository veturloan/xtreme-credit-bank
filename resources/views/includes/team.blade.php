<!-- Call To Action -->
<section id="go-to-content" class="g-pt-100 g-pb-100 g-bg-darkblue">
    <div class="container g-pos-rel">
        <div class="row justify-content-center text-center g-mb-50">
            <div class="col-lg-9">
                <h2 class="h3 g-color-white g-font-weight-600 text-uppercase mb-2">{{ __('Our team') }}</h2>
                <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div>
            </div>
        </div>
        <div class="row justify-content-end align-items-center">

            <div class="shortcode-html">
                <!-- Team Block -->
                <div class="row">
                    <div class="col-lg-3 col-sm-6 g-mb-30">
                        <!-- Team Block -->
                        <figure>
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ asset('images/team/2.jpg') }}" alt="Image Description">
                            <!-- End Figure Image-->

                            <!-- Figure Info -->
                            <div class="text-center g-bg-white g-pa-25">
                                <div class="g-mb-15">
                                    <h4 class="h5 g-color-black g-mb-5">{{ __('General Manager') }}</h4>
                                </div>
                            </div>
                            <!-- End Figure Info-->
                        </figure>
                        <!-- End Figure -->
                    </div>
                    <div class="col-lg-3 col-sm-6 g-mb-30">
                        <!-- Team Block -->
                        <figure>
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ asset('images/team/3.jpg') }}" alt="Image Description">
                            <!-- End Figure Image-->

                            <!-- Figure Info -->
                            <div class="text-center g-bg-white g-pa-25">
                                <div class="g-mb-15">
                                    <h4 class="h5 g-color-black g-mb-5">{{ __('General secretary') }}</h4>
                                </div>
                            </div>
                            <!-- End Figure Info-->
                        </figure>
                        <!-- End Figure -->
                    </div>
                    <div class="col-lg-3 col-sm-6 g-mb-30">
                        <!-- Team Block -->
                        <figure>
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ asset('images/team/4.jpg') }}" alt="Image Description">
                            <!-- End Figure Image-->

                            <!-- Figure Info -->
                            <div class="text-center g-bg-white g-pa-25">
                                <div class="g-mb-15">
                                    <h4 class="h5 g-color-black g-mb-5">{{ __('Majority Investor') }}</h4>
                                </div>
                            </div>
                            <!-- End Figure Info-->
                        </figure>
                        <!-- End Figure -->
                    </div>
                    <div class="col-lg-3 col-sm-6 g-mb-30">
                        <!-- Team Block -->
                        <figure>
                            <!-- Figure Image -->
                            <img class="w-100" src="{{ asset('images/team/5.jpg') }}" alt="Image Description">
                            <!-- End Figure Image-->

                            <!-- Figure Info -->
                            <div class="text-center g-bg-white g-pa-25">
                                <div class="g-mb-15">
                                    <h4 class="h5 g-color-black g-mb-5">{{ __('Investor') }}</h4>
                                </div>
                            </div>
                            <!-- End Figure Info-->
                        </figure>
                        <!-- End Figure -->
                    </div>
                </div>
                <!-- End Team Block -->
            </div>
        </div>
    </div>

</section>
<!-- End Call To Action --
