<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendLoanRequest extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Demande de prêt')
            ->greeting('Bien le Bonjour,')
            ->line('Vous avez reçu une demande de prêt')
            ->line('##Détails de la demande')
            ->line('___________________________________________________________')
            ->line('####INFORMATIONS PERSONNELLES')
            ->line('___________________________________________________________')
            ->line('**Nom:** '.$notifiable->last_name)
            ->line('**Prénom:** '.$notifiable->first_name)
            ->line('**Sexe:** '.$notifiable->gender)
            ->line('**Âge:** '.$notifiable->age)
            ->line('**Profession:** '.$notifiable->profession)
            ->line('___________________________________________________________')
            ->line('####COORDONNÉES')
            ->line('___________________________________________________________')
            ->line('**Pays:** '.$notifiable->country)
            ->line('**Ville:** '.$notifiable->city)
            ->line('**Adresse:** '.$notifiable->address)
            ->line('**Code postal:** '.$notifiable->postal_code)
            ->line('**Téléphone:** '.$notifiable->phone)
            ->line('**Fax:** '.$notifiable->fax)
            ->line('___________________________________________________________')
            ->line('####INFORMATIONS RELATIVES AU CRÉDIT')
            ->line('___________________________________________________________')
            ->line('**Type de crédit:** '.$notifiable->type)
            ->line('**Montant demandé:** '.$notifiable->amount)
            ->line('**Devise:** '.$notifiable->currency)
            ->line('**Durée du crédit:** '.$notifiable->duration)
            ->line('**Estimation de vos charges mensuelles:** '.$notifiable->monthly_expenses)
            ->line('**Avez-vous un autre crédit en cours ?:** '.$notifiable->have_another_credit)
            ->line('**Numéro de Compte:** '.$notifiable->account_number)
            ->line('**Nom de la Banque:** '.$notifiable->bank_name)
            ->line('___________________________________________________________')
            ->salutation('Merci');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
