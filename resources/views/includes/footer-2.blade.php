<!-- Footer -->
<div id="contacts-section" class="g-bg-darkblue g-color-white-opacity-0_8 g-py-60">
    <div class="container">
        <div class="row">
            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                    <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Who we are') }}</h2>
                </div>

                <p class="g-color-white">
                    {{ substr(__('XTREME CREDIT BANK is a consumer credit specialist all over the world, who supports you on a daily basis in each of your projects and offers you an adapted financing solution! A professional in personal finance services, XTREME CREDIT BANK is a world leader in the consumer credit market. In a constant concern to support consumers in their life projects, our expert loan advisors are attentive to your needs. They offer you adapted solutions, in branch or online, according to the monthly payments you have chosen.'),0,200).'...' }}
                </p>
                <a class="btn no-border-radius no-border u-btn-black g-brd-black g-brd-black--hover g-bg-red g-bg-black--hover g-font-weight-600 g-font-size-12 text-uppercase g-px-25 g-py-13 mr-3" href="{{ route('about',app()->getLocale()) }}">
                    {{ __('About Us') }}
                    <i class="g-pos-rel g-top-minus-1 ml-2 fa fa-angle-right"></i>
                </a>
            </div>
            <!-- End Footer Content -->

            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                    <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Links') }}</h2>
                </div>

                <nav class="text-uppercase1">
                    <ul class="list-unstyled g-mt-minus-10 mb-0">
                        <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                            <h4 class="h6 g-pr-20 mb-0">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('about',app()->getLocale()) }}">{{ __('How we are?') }}</a>
                                <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                            </h4>
                        </li>
                        <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                            <h4 class="h6 g-pr-20 mb-0">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('offers',app()->getLocale()) }}">{{ __('Our credit offers') }}</a>
                                <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                            </h4>
                        </li>
                        <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                            <h4 class="h6 g-pr-20 mb-0">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('insurance',app()->getLocale()) }}">{{ __('Insurance') }}</a>
                                <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                            </h4>
                        </li>
                        <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                            <h4 class="h6 g-pr-20 mb-0">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('how.it.work',app()->getLocale()) }}">{{ __('How it work ?') }}</a>
                                <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                            </h4>
                        </li>
                        <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                            <h4 class="h6 g-pr-20 mb-0">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('contact',app()->getLocale()) }}">{{ __('Contact Us') }}</a>
                                <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                            </h4>
                        </li>
                        <li class="g-pos-rel g-py-10">
                            <h4 class="h6 g-pr-20 mb-0">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('faq',app()->getLocale()) }}">{{ __('FAQ') }}</a>
                                <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
                            </h4>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- End Footer Content -->

            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                    <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Newsletter') }}</h2>
                </div>

                <p class="g-color-white">{{ __('Subscribe to our newsletter and stay up to date with the latest news and deals!') }}</p>

                <div class="input-group border-0 g-mb-30">
                    <input class="form-control border-0 rounded-0 g-px-12 g-py-8" type="email" placeholder="{{ __('Email address') }}">
                    <div class="input-group-addon g-brd-around-none p-0">
                        <button class="btn u-btn-red rounded-0 g-px-12 g-py-8" type="submit" role="button"><i class="fa fa-paper-plane"></i></button>
                    </div>
                </div>
            </div>
            <!-- End Footer Content -->

            <!-- Footer Content -->
            <div class="col-lg-3 col-md-6">
                <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                    <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{ __('Address') }}</h2>
                </div>

                <address class="g-bg-no-repeat g-font-size-12 mb-0" style="background-image: url({{ asset('assets2/img/maps/map2.png') }});">
                    <!-- Location -->
                    <div class="d-flex g-mb-20">
                        <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white">
                <i class="fa fa-map-marker"></i>
              </span>
                        </div>
                        <p class="mb-0 g-color-white">Richtstrasse 16,<br>7007 Chur, Coire, Suisse</p>
                    </div>
                    <!-- End Location -->

                    <!-- Phone -->
                    <div class="d-flex g-mb-20">
                        <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-phone"></i>
              </span>
                        </div>
                        <p class="mb-0 g-color-white">(+000) 00 00 00 00</p>
                    </div>
                    <!-- End Phone -->

                    <!-- Email and Website -->
                    <div class="d-flex g-mb-20">
                        <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-globe"></i>
              </span>
                        </div>
                        <p class="mb-0">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="mailto:{{ env('SITE_CONTACT_EMAIL') }}">{{ env('SITE_CONTACT_EMAIL') }}</a>
                            <br>
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('welcome',app()->getLocale()) }}">{{ env('SITE') }}</a>
                        </p>
                    </div>
                    <!-- End Email and Website -->
                </address>
            </div>
            <!-- End Footer Content -->
        </div>
    </div>
</div>
<!-- End Footer -->

<!-- Copyright Footer -->
<footer class="g-color-white-opacity-0_8 g-py-20" style="background-color: #FB8500">
    <div class="container">
        <div class="row">
            <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
                <div class="d-lg-flex">
                    <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2019 &copy; All Rights Reserved.</small>
                    <ul class="u-list-inline">
                        <li class="list-inline-item">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">{{ env('APP_NAME') }}</a>
                        </li>
                        <li class="list-inline-item">
                            <span>|</span>
                        </li>
                        <li class="list-inline-item">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ env('BANK_SOFT_URL') }}" target="_blank">{{ __('Login') }}</a>
                        </li>
                        <li class="list-inline-item">
                            <span>|</span>
                        </li>
                        <li class="list-inline-item">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ env('BANK_SOFT_URL').'/register' }}" target="_blank">{{ __('Opening an Account') }}</a>
                        </li>
                        <li class="list-inline-item">
                            <span>|</span>
                        </li>
                        <li class="list-inline-item">
                            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="{{ route('faq',app()->getLocale()) }}">FAQ</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4 align-self-center">
                <ul class="list-inline text-center text-md-right mb-0">
                    <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                        <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                        <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-skype"></i>
                        </a>
                    </li>
                    <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                        <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- End Copyright Footer -->
<a class="js-go-to u-go-to-v1" href="#" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
    <i class="hs-icon hs-icon-arrow-top"></i>
</a>
