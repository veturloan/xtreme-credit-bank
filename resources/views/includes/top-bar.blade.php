<!-- Top Bar -->
<div class="u-header__section u-header__section--hidden u-header__section--dark g-transition-0_3 g-py-10" style="background-color: #FB8500">
    <div class="container">
        <div class="row flex-column flex-sm-row justify-content-between align-items-center text-uppercase g-font-weight-400 g-color-white g-font-size-12 g-mx-0--lg">
            {{--<div class="col-auto">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item text-lowercase">
                        {{ __('a credit that makes you enjoy') }}
                    </li>
                </ul>
            </div>--}}

            {{--<div class="col-auto">
                <i class="fa fa-phone g-font-size-15 g-valign-middle g-color-white g-mr-3 g-mt-minus-2"></i>
                <i class="fa fa-whatsapp g-font-size-15 g-valign-middle g-color-green-light g-mr-3 g-mt-minus-2"></i>
                {{ env('SITE_PHONE') }}
            </div>--}}

            <div class="col-auto text-lowercase g-font-size-15">
                <i class="fa fa-at g-font-size-15 g-valign-middle g-color-white g-mr-3 g-mt-minus-2"></i>
                {{ env('SITE_CONTACT_EMAIL') }}
            </div>

            <div class="col-auto g-pos-rel">
                <ul class="list-inline g-overflow-hidden g-pt-1 g-mx-minus-4 mb-0 text-center">
                    <li class="list-inline-item g-mx-4">
                        <a href="#" id="languages-dropdown-invoker-2" class="g-color-white g-color-primary--hover g-text-underline--none--hover" aria-controls="languages-dropdown-2" aria-haspopup="true" aria-expanded="false" data-dropdown-event="click" data-dropdown-target="#languages-dropdown-2"
                           data-dropdown-type="css-animation" data-dropdown-duration="300" data-dropdown-hide-on-scroll="false" data-dropdown-animation-in="fadeIn" data-dropdown-animation-out="fadeOut">
                            <img src="{{ config('lang_list.flags.'.app()->getLocale()) }}" alt="{{ config('lang_list.languages.'.app()->getLocale()) }}" height="11" class="mr-1">
                            {{ config('lang_list.languages.'.app()->getLocale()) }}
                        </a>
                        <ul id="languages-dropdown-2" class="list-unstyled g-pos-abs g-left-0 g-bg-black g-width-200 g-pb-5 g-mt-10 text-left" aria-labelledby="languages-dropdown-invoker-2">
                            @foreach(config('lang_list.languages') as $k => $v)
                                <li>
                                    <a class="d-block g-color-white g-color-primary--hover g-text-underline--none--hover g-py-5 g-px-20" href="{{ route(Route::currentRouteName(),$k) }}">
                                        <img src="{{ config('lang_list.flags')[$k] }}" alt="{{ $v }}" height="11" class="mr-1">
                                        {{ $v }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="list-inline-item g-mx-4">|</li>
                    <li class="list-inline-item g-mx-4">
                        <a class="g-color-white g-color-primary--hover g-text-underline--none--hover" href="{{ env('BANK_SOFT_URL').'/register' }}" target="_blank">{{ __('Opening an Account') }}</a>
                    </li>
                    <li class="list-inline-item g-mx-4">|</li>
                    <li class="list-inline-item g-mx-4">
                        <a class="g-color-white g-color-primary--hover g-text-underline--none--hover" href="{{ route('faq',app()->getLocale()) }}">FAQ</a>
                    </li>
                    <li class="list-inline-item g-mx-4">|</li>
                    <li class="list-inline-item g-mx-4">
                        <a class="g-color-white g-color-primary--hover g-text-underline--none--hover" href="{{ route('contact',app()->getLocale()) }}">{{ __('Contact Us') }}</a>
                    </li>
                    <li class="list-inline-item g-mx-4 hide-on-mobile">|</li>
                    <li class="list-inline-item g-mx-4 hide-on-mobile">
                        <a class="u-link-v5 g-color-white g-bg-blue g-font-weight-500 g-px-10 g-py-10" href="{{ env('BANK_SOFT_URL') }}" target="_blank">
                            <i class="fa fa-user-circle mr-2"></i>
                            Login
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <div class="row show-on-mobile justify-content-center">
            <a class="u-link-v5 g-color-white g-bg-blue g-font-weight-500 g-px-20 g-py-5 border-white border" href="{{ env('BANK_SOFT_URL') }}" target="_blank">
                <i class="fa fa-user-circle mr-2"></i>
                Login
            </a>
        </div>
    </div>
</div>
<!-- End Top Bar -->
