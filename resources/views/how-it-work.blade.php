@extends('layouts.frontend-2')

@section('title')

    {{ __('How it work ?') }}

@endsection

@section('content')

    <section class="container g-pt-100 g-mt-100 g-pb-100">
        <header class="text-center g-mb-60">
            <div class="u-heading-v2-3--bottom g-brd-primary g-mb-20">
                <h2 class="h3 text-uppercase g-font-weight-300 u-heading-v2__title">{{ __('How it work ?') }}</h2>
            </div>
            <p class="lead g-px-200--lg">{{ __("Finie les demandes de crédit cauchemardesques. En un clin d'oeil, votre argent tombe dans vos mains : c'est clair et simple, rien ne vous est caché !") }}</p>
        </header>
        <div class="row">
            <div class="col-lg-8 align-self-center g-mb-50 g-mb-0--lg">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="media g-mb-25">
                            <div class="d-flex mr-4">
                                <span class="u-icon-v3 u-icon-size--sm g-bg-pink g-color-white g-rounded-50x">
                                  <i class="icon-check"></i>
                                </span>
                            </div>
                            <div class="media-body">
                                <h3 class="h5 g-mb-5">
                                    <a href="#" class="g-color-main">{{ __('Simulate your credit') }}</a>
                                </h3>
                                <p>{{ __('Simply find the data you need to optimize your loan: loan amount, monthly payment, term or credit rate.') }}</p>
                            </div>
                        </div>

                        <div class="media g-mb-25">
                            <div class="d-flex mr-4">
            <span class="u-icon-v3 u-icon-size--sm g-bg-primary g-color-white g-rounded-50x">
              <i class="icon-like"></i>
            </span>
                            </div>
                            <div class="media-body">
                                <h3 class="h5 g-mb-5">
                                    <a href="#" class="g-color-main">{{ __('Choose your loan type') }}</a>
                                </h3>
                                <p>{{ __('Browse our list of loan offers and find the credit that best suits your project.') }}</p>
                            </div>
                        </div>

                        <div class="media g-mb-25">
                            <div class="d-flex mr-4">
            <span class="u-icon-v3 u-icon-size--sm g-bg-gray-dark-v3 g-color-white g-rounded-50x">
              <i class="icon-list"></i>
            </span>
                            </div>
                            <div class="media-body">
                                <h3 class="h5 g-mb-5">
                                    <a href="#" class="g-color-main">{{ __('Fill the request form') }}</a>
                                </h3>
                                <p>{{ __('Submit the necessary information about your identity, contact details and credit needs.') }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="media g-mb-25">
                            <div class="d-flex mr-4">
            <span class="u-icon-v3 u-icon-size--sm g-bg-aqua g-color-white g-rounded-50x">
              <i class="icon-bubbles"></i>
            </span>
                            </div>
                            <div class="media-body">
                                <h3 class="h5 g-mb-5">
                                    <a href="#" class="g-color-main">{{ __('Talk to a personal advisor') }}</a>
                                </h3>
                                <p>{{ __('Immediately after submitting your loan application, a specialist advisor will contact you to assist you during the procedure.') }}</p>
                            </div>
                        </div>

                        <div class="media g-mb-25">
                            <div class="d-flex mr-4">
            <span class="u-icon-v3 u-icon-size--sm g-bg-purple g-color-white g-rounded-50x">
              <i class="icon-folder"></i>
            </span>
                            </div>
                            <div class="media-body">
                                <h3 class="h5 g-mb-5">
                                    <a href="#" class="g-color-main">{{ __('Complete your application') }}</a>
                                </h3>
                                <p>{{ __('Provide the rest of the important elements for the constitution of your loan file according to your credit advisor.') }}</p>
                            </div>
                        </div>

                        <div class="media g-mb-25">
                            <div class="d-flex mr-4">
            <span class="u-icon-v3 u-icon-size--sm g-bg-deeporange g-color-white g-rounded-50x">
              <i class="fa fa-money"></i>
            </span>
                            </div>
                            <div class="media-body">
                                <h3 class="h5 g-mb-5">
                                    <a href="#" class="g-color-main">{{ __('Receive your credit') }}</a>
                                </h3>
                                <p>{{ __('Once your file has been validated, without delay, you will be able to enter your bank details to receive your credit very easily.') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 text-center text-lg-left">
                <img class="img-fluid g-width-300" src="{{ __('images/how-it-work.jpg') }}" alt="Image Description">
            </div>
        </div>
    </section>

@endsection
