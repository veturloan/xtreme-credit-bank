<!-- About -->
<div class="container g-pt-100 g-pb-70">
    <div class="row justify-content-between align-items-center">
        <div class="col-lg-7 g-mb-50">
            <!-- About Info -->
            <div class="mb-5">
                <span class="d-block text-uppercase g-color-dark-light-v1 g-font-weight-500 g-font-size-13 mb-4">{{ __('What is XTREME CREDIT BANK?') }}</span>
                <h2 class="mb-4">{{ __('Your online credit expert.') }}</h2>

                <div class="row">
                    <div class="col-sm-3">
                        <!-- Nav tabs -->
                        <ul class="nav flex-column" role="tablist" data-target="nav-1-1-accordion-primary-ver">
                            <li class="nav-item">
                                <a class="nav-link active g-color-primary--hover g-color-primary--parent-active g-pl-0" data-toggle="tab" href="#nav-1-1-accordion-primary-ver--1" role="tab">{{ __('Who we are') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link g-color-primary--hover g-color-primary--parent-active g-pl-0" data-toggle="tab" href="#nav-1-1-accordion-primary-ver--2" role="tab">{{ __('Our vision') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link g-color-primary--hover g-color-primary--parent-active g-pl-0" data-toggle="tab" href="#nav-1-1-accordion-primary-ver--3" role="tab">{{ __('Our mission') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link g-color-primary--hover g-color-primary--parent-active g-pl-0" data-toggle="tab" href="#nav-1-1-accordion-primary-ver--4" role="tab">{{ __('Our History') }}</a>
                            </li>
                        </ul>
                        <!-- End Nav tabs -->
                    </div>

                    <div class="col-sm-9">
                        <!-- Tab panes -->
                        <div id="nav-1-1-accordion-primary-ver" class="tab-content">
                            <div class="tab-pane fade show active" id="nav-1-1-accordion-primary-ver--1" role="tabpanel">
                                <p class="g-line-height-2">
                                    {{ __('XTREME CREDIT BANK is a consumer credit specialist all over the world, who supports you on a daily basis in each of your projects and offers you an adapted financing solution! A professional in personal finance services, XTREME CREDIT BANK is a world leader in the consumer credit market. In a constant concern to support consumers in their life projects, our expert loan advisors are attentive to your needs. They offer you adapted solutions, in branch or online, according to the monthly payments you have chosen.') }}
                                </p>
                            </div>

                            <div class="tab-pane fade" id="nav-1-1-accordion-primary-ver--2" role="tabpanel">
                                <p class="g-line-height-2">
                                    {{ __('Being a partner company means supporting its customers at every stage of their lives and offering them the highest quality. It means guaranteeing clear and fast answers to each request. It means having a team of real credit specialists, constantly trained. Being a responsible credit company also means accepting the strictest controls. XTREME CREDIT BANK is committed to this.') }}
                                </p>
                            </div>

                            <div class="tab-pane fade" id="nav-1-1-accordion-primary-ver--3" role="tabpanel">
                                <p class="g-line-height-2">
                                    {{ __('XTREME CREDIT BANK is committed as a responsible lender by offering loans adapted to the needs of customers and their budgets, and by training and raising awareness among XTREME CREDIT BANK employees of the issues related to the prevention of over-indebtedness and the risk of financial exclusion.') }}
                                </p>
                            </div>

                            <div class="tab-pane fade" id="nav-1-1-accordion-primary-ver--4" role="tabpanel">
                                <p class="g-line-height-2">
                                    {{ __('At your side for more than 20 years. At the end of the founding of the XTREME CREDIT BANK-GROUP, XTREME CREDIT BANK was created in 2008 to meet the equipment needs of households.  The credit company was globalized in 2012 and then privatized in 2013. In 2015, it was attached to the Crédit Mutuel group through a partnership, before being officially acquired in 2017.') }}
                                </p>
                            </div>
                        </div>
                        <!-- End Tab panes -->
                    </div>
                </div>
                <!-- End Tab -->
            </div>

            <a class="btn no-border-radius no-border u-btn-black g-brd-black g-brd-black--hover g-bg-red g-bg-black--hover g-font-weight-600 g-font-size-12 text-uppercase g-px-25 g-py-13 mr-3" href="{{ route('about',app()->getLocale()) }}">
                {{ __('About Us') }}
                <i class="g-pos-rel g-top-minus-1 ml-2 fa fa-angle-right"></i>
            </a>
            <a class="g-brd-bottom g-brd-main g-brd-primary--hover g-color-main g-color-primary--hover g-font-size-12 text-uppercase g-text-underline--none--hover g-pb-3" href="{{ route('contact',app()->getLocale()) }}">
                {{ __('Ask a Question') }}
            </a>
            <!-- End About Info -->
        </div>

        <div class="col-sm-8 col-md-6 col-lg-4 mx-auto g-mb-50">
            <figure class="text-center">
                <!-- Figure Image -->
                <img class="mx-auto" src="{{ asset('images/team/1.png') }}" alt="Image Description">
                <!-- End Figure Image-->

                <!-- Figure Info -->
                {{--<div class="text-center g-bg-white g-pa-25">
                    <div class="g-mb-15">
                        <h4 class="h5 g-color-black g-mb-5">Laurent MICHEL</h4>
                        <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">{{ __('President of the Bank') }}</em>
                    </div>
                </div>--}}
                <!-- End Figure Info-->
            </figure>
        </div>
    </div>
</div>
<!-- End About -->
