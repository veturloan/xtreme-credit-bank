<!-- Promo Banner -->
<div class="g-bg-img-hero g-bg-pos-top-center g-bg-size-cover g-bg-cover g-bg-white-gradient-opacity-v5--after" style="background-image: url({{ url('images/img3_1920x800.jpg') }});">
    <div class="container g-pos-rel g-z-index-1">
        <div class="row justify-content-between">
            <div class="col-md-5 g-py-100">
                <div class="mb-5">
                    <span class="d-block text-uppercase g-color-red g-font-weight-500 g-font-size-13 mb-4">{{ __('What are you waiting for now?') }}</span>
                    <h2 class="mb-4 g-color-black">{{ __('Take the plunge now!') }}<br>&#171;{{ __('Your future is at stake.') }}&#187;</h2>
                    <p class="g-font-size-16 g-line-height-2">{{ __("You fill out our business credit application form in less than 7 minutes. It's completely free and without obligation!") }}</p>
                </div>
                <a class="btn u-btn-black g-brd-main g-brd-primary--hover g-bg-black g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-px-25 g-py-13" href="{{ route('request',app()->getLocale()) }}">
                    {{ __("I'm going to start") }}
                    <i class="g-pos-rel g-top-minus-1 ml-2 fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Promo Banner -->
