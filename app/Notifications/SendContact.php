<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendContact extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Formulaire de Contact')
            ->greeting('Bien le Bonjour,')
            ->line('Vous avez reçu un message depuis le formulaire de contact')
            ->line('###Détails du Message')
            ->line('**Nom:** '.$notifiable->contact_last_name)
            ->line('**Prénom:** '.$notifiable->contact_first_name)
            ->line('**Email:** '.$notifiable->contact_email)
            ->line('**Téléphone:** '.$notifiable->contact_phone)
            ->line('**Sujet:** '.$notifiable->contact_subject)
            ->line('**Message:**')
            ->line('___________________________________________________________')
            ->line('*'.$notifiable->contact_message.'*')
            ->line('___________________________________________________________')
            ->salutation('Merci');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
