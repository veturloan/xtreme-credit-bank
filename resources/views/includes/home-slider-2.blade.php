<!-- Revolution Slider -->
<div class="g-overflow-hidden">
    <div id="rev_slider_1086_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="woobig1" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 auto mode -->
        <div id="rev_slider_1086_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul><!-- SLIDE  -->
                <li data-index="rs-3076" data-transition="slideremovehorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="assets/img-temp/1920x1080/img5.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('images/sliders/slide-bg/home-3.jpg') }}" alt="Image description" data-lazyload="assets/img-temp/1920x1080/img5.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                         id="slide-3076-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                         data-y="['top','top','top','top']" data-voffset="['30','30','30','30']"
                         data-width="['430','430','430','420']"
                         data-height="390"
                         data-whitespace="nowrap"

                         data-type="shape"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power3.easeOut"},{"delay":"wait","speed":1200,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 5;text-transform:left;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Woo-TitleLarge   tp-resizeme"
                         id="slide-3076-layer-2"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['60','60','60','60']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":600,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 6; min-width: 370px; max-width: 370px; white-space: normal;text-transform:left;">
                        {{ env('APP_NAME') }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Woo-Rating tp-resizeme g-font-size-18"
                         id="slide-3076-layer-6"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['160','160','160','160']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":700,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; min-width: 370px; max-width: 370px; white-space: normal; line-height: 22px;text-transform:left;">
                        {{ __('Your Bank') }}
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption Woo-Rating tp-resizeme g-font-size-25"
                         id="slide-3076-layer-9"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['225','225','225','225']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":800,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 8; min-width: 370px; max-width: 370px; white-space: normal; line-height: 25px;text-transform:left;">
                        {{ __('Excellence at your service.') }}
                    </div>

                    <!-- LAYER NR. 7 -->
                    <a class="tp-caption Woo-ProductInfo rev-btn  tp-resizeme g-brd-gray-light-v4 g-bg-darkblue g-color-white"
                         id="slide-3076-layer-14"
                         data-x="['left','left','left','left']" data-hoffset="['115','115','115','108']"
                         data-y="['top','top','top','top']" data-voffset="['300','300','300','301']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1100,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(221, 221, 221, 1.00);bw:1px 1px 1px 1px;"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[75,75,75,75]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[50,50,50,50]"
                        href="{{ env('BANK_SOFT_URL').'/register' }}" target="_blank"
                         style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;border-radius:0">
                        <i class="pe-7s-add-user" style="font-size:25px; float: left; margin-top: -6px; margin-right: 6px;"></i>
                        {{ __('Opening an Account') }}
                    </a>

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption Woo-AddToCart rev-btn  tp-resizeme g-brd-primary g-color-white g-bg-red"
                         id="slide-3076-layer-13"
                         data-x="['left','left','left','left']" data-hoffset="['167','167','167','162']"
                         data-y="['top','top','top','top']" data-voffset="['351','351','351','351']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(243, 168, 71, 1.00);bw:1px 1px 1px 1px;"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[75,75,75,75]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[50,50,50,50]"
                        href="{{ env('BANK_SOFT_URL') }}" target="_blank"
                         style="z-index: 12; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;border-radius:0">
                        <i class="pe-7s-lock" style="font-size:25px; float: left; margin-top: -6px; margin-right: 6px;"></i>
                        {{ __('Login') }}
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-3077" data-transition="slideremovehorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="assets/img-temp/1920x1080/img6.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('images/sliders/slide-bg/home-4.jpg') }}" alt="Image description" data-lazyload="assets/img-temp/1920x1080/img6.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                         id="slide-3076-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                         data-y="['top','top','top','top']" data-voffset="['30','30','30','30']"
                         data-width="['430','430','430','420']"
                         data-height="390"
                         data-whitespace="nowrap"

                         data-type="shape"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power3.easeOut"},{"delay":"wait","speed":1200,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 5;text-transform:left;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Woo-TitleLarge   tp-resizeme"
                         id="slide-3076-layer-2"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['60','60','60','60']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":600,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 6; min-width: 370px; max-width: 370px; white-space: normal;text-transform:left;">
                        XTREME Mobile
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Woo-Rating tp-resizeme g-font-size-18"
                         id="slide-3076-layer-6"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['160','160','160','160']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":700,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; min-width: 370px; max-width: 370px; white-space: normal; line-height: 22px;text-transform:left;">
                        {{ __('Banking Mobility') }}
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption Woo-Rating tp-resizeme g-font-size-25"
                         id="slide-3076-layer-9"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['225','225','225','225']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":800,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 8; min-width: 370px; max-width: 370px; white-space: normal; line-height: 25px;text-transform:left;">
                        {{ __('Your money is following you everywhere and safely.') }}
                    </div>

                    <!-- LAYER NR. 7 -->
                    <a class="tp-caption Woo-ProductInfo rev-btn  tp-resizeme g-brd-gray-light-v4 g-bg-darkblue g-color-white"
                         id="slide-3076-layer-14"
                         data-x="['left','left','left','left']" data-hoffset="['115','115','115','108']"
                         data-y="['top','top','top','top']" data-voffset="['300','300','300','301']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1100,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(221, 221, 221, 1.00);bw:1px 1px 1px 1px;"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[75,75,75,75]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[50,50,50,50]"
                        href="{{ route('card.request',app()->getLocale()) }}"
                         style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;border-radius:0">
                        <i class="pe-7s-credit" style="font-size:25px; float: left; margin-top: -6px; margin-right: 6px;"></i>
                        {{ __('Request a credit card') }}
                    </a>

                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
</div>
<!-- End Revolution Slider -->

<!-- Icon Blocks -->
<div class="row no-gutters g-brd-bottom--lg g-brd-gray-light-v4">
    <div class="col-lg-4 g-pa-60">
        <!-- Icon Blocks -->
        <div class="text-center">
          <span class="d-inline-block u-icon-v3 u-icon-size--xl g-font-size-36 g-bg-darkblue g-color-white rounded-circle g-mb-20">
              <i class="icon-finance-086 u-line-icon-pro"></i>
            </span>
            <h3 class="h4 g-color-gray-dark-v2 mb-3">{{ __('Our vision') }}</h3>
            <p class="mb-0">
                {{ __('Being a partner company means supporting its customers at every stage of their lives and offering them the highest quality. It means guaranteeing clear and fast answers to each request. It means having a team of real credit specialists, constantly trained. Being a responsible credit company also means accepting the strictest controls. XTREME CREDIT BANK is committed to this.') }}
            </p>
        </div>
        <!-- End Icon Blocks -->
    </div>

    <div class="col-lg-4 g-brd-left--lg g-brd-gray-light-v4 g-pa-60">
        <!-- Icon Blocks -->
        <div class="text-center">
          <span class="d-inline-block u-icon-v3 u-icon-size--xl g-font-size-36 g-bg-darkblue g-color-white rounded-circle g-mb-20">
              <i class="icon-education-031 u-line-icon-pro"></i>
            </span>
            <h3 class="h4 g-color-gray-dark-v2 mb-3">{{ __('Our mission') }}</h3>
            <p class="mb-0">
                {{ __('XTREME CREDIT BANK is committed as a responsible lender by offering loans adapted to the needs of customers and their budgets, and by training and raising awareness among XTREME CREDIT BANK employees of the issues related to the prevention of over-indebtedness and the risk of financial exclusion.') }}
            </p>
        </div>
        <!-- End Icon Blocks -->
    </div>

    <div class="col-lg-4 g-brd-left--lg g-brd-gray-light-v4 g-pa-60">
        <!-- Icon Blocks -->
        <div class="text-center">
          <span class="d-inline-block u-icon-v3 u-icon-size--xl g-font-size-36 g-bg-darkblue g-color-white rounded-circle g-mb-20">
              <i class="icon-finance-222 u-line-icon-pro"></i>
            </span>
            <h3 class="h4 g-color-gray-dark-v2 mb-3">{{ __('Our History') }}</h3>
            <p class="mb-0">
                {{ __('At your side for more than 20 years. At the end of the founding of the XTREME CREDIT BANK-GROUP, XTREME CREDIT BANK was created in 2008 to meet the equipment needs of households.  The credit company was globalized in 2012 and then privatized in 2013. In 2015, it was attached to the Crédit Mutuel group through a partnership, before being officially acquired in 2017.') }}
            </p>
        </div>
        <!-- End Icon Blocks -->
    </div>
</div>
<!-- End Icon Blocks -->

<!-- About Us -->
<section class="g-py-100" style="background-color: #f0f0f0">
    <div class="container">
        <header class="text-center g-width-60x--md mx-auto g-mb-60">
            <div class="u-heading-v2-3--bottom g-brd-primary g-mb-20">
                <h2 class="h3 u-heading-v2__title g-color-gray-dark-v2 text-uppercase g-font-weight-600">{{ __('How we are?') }}</h2>
            </div>
            <p class="lead">
                {{ __('What is XTREME CREDIT BANK?') }}
            </p>
        </header>

        <div class="row">
            <div class="col-lg-4 align-self-center">
                <img src="{{ asset('images/logo/logo-footer.png') }}" alt="{{ env('APP_NAME') }}" height="330">
            </div>

            <div class="col-lg-8 align-self-center g-pl-30--lg g-mb-50 g-mb-0--lg">
                <p class="g-mb-30">
                    {{ __('XTREME CREDIT BANK is a consumer credit specialist all over the world, who supports you on a daily basis in each of your projects and offers you an adapted financing solution! A professional in personal finance services, XTREME CREDIT BANK is a world leader in the consumer credit market. In a constant concern to support consumers in their life projects, our expert loan advisors are attentive to your needs. They offer you adapted solutions, in branch or online, according to the monthly payments you have chosen.') }}
                </p>
            </div>
        </div>
    </div>
</section>
<!-- End About Us -->
