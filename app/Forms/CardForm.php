<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CardForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('type', 'select',[
                'choices' => [
                    "Visa Classic" => "Visa Classic",
                    "Visa Gold" => "Visa Gold",
                    "Visa Infinite" => "Visa Infinite"
                ],
                'empty_value' => '==='.__('Choose credit card type').'===',
                'label' => __('Credit card type'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'class' => 'js-custom-select w-100 u-select-v2 u-shadow-v19 g-color-black g-color-primary--hover g-bg-white text-left g-rounded-30 g-px-15 g-py-5'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('last_name','text',[
                'label' => __('Last Name'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your last name...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('first_name','text',[
                'label' => __('First Name'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your first name...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('spoken_languages','text',[
                'label' => __('Spoken languages'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('List the languages you speak...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('country','text',[
                'label' => __('Country'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your country...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('city','text',[
                'label' => __('City'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your city...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('address','text',[
                'label' => __('Address'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your address...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('postal_code','text',[
                'label' => __('Postal code'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your postal code...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'nullable',
                    'string',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('email','email',[
                'label' => __('E-mail Address'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your e-mail address...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'email',
                    'max:250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add('phone','text',[
                'label' => __('Telephone'),
                'label_attr' => [
                    'class' => 'g-color-gray-dark-v2 g-font-size-13'
                ],
                'attr' => [
                    'placeholder' => __('Your phone number...'),
                    'class' => 'form-control u-shadow-v19 g-bg-white g-font-size-14 g-rounded-30 g-px-15 g-py-5 g-mb-15'
                ],
                'rules' => [
                    'required',
                    'string',
                    'numeric',
                    'digits_between:7,250'
                ],
                'wrapper' => [
                    'class' => 'col-md-12 form-group g-mb-15'
                ]
            ])
            ->add(__('Continue'), 'submit',[
                'attr' => [
                    'class' => 'btn u-btn-black g-brd-main g-brd-primary--hover g-bg-main g-bg-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-px-100 g-py-13'
                ],
                'wrapper' => [
                    'class' => 'text-center'
                ]
            ]);
    }
}
