<!-- Our Pricing -->
<section class="g-bg-secondary g-py-100">
    <div class="container">
        <header class="text-center g-width-60x--md mx-auto g-mb-60">
            <div class="u-heading-v2-3--bottom g-brd-primary g-mb-20">
                <h2 class="h3 u-heading-v2__title g-color-gray-dark-v2 text-uppercase g-font-weight-600">{{ __('Our Services') }}</h2>
            </div>
        </header>

        <div class="row align-items-lg-center">
            <div class="col-lg-12 col-md-12">
                <!-- Row -->
                <div class="row g-mr-20--md g-mb-30--lg">
                    <div class="col-lg-4 g-mb-40">
                        <i class="icon-education-087 u-line-icon-pro d-block g-font-size-55 g-line-height-1 g-color-darkblue g-mb-15"></i>
                        <h4 class="h4 g-color-gray-dark-v2 g-mb-10">{{ __('Insurance') }}</h4>
                        <p class="mb-0">
                            {{ __('XTREME CREDIT BANK supports you on a daily basis, because it is crucial for your needs. Protect yourself in the event of accidental death or disability.') }}
                        </p>
                    </div>

                    <div class="col-lg-4 g-mb-40">
                        <i class="icon-education-035 u-line-icon-pro d-block g-font-size-55 g-line-height-1 g-color-darkblue g-mb-15"></i>
                        <h4 class="h4 g-color-gray-dark-v2 g-mb-10">{{ __('Funding') }}</h4>
                        <p class="mb-0">
                            {{ __('To put your loan application online, you must meet certain conditions, such as: Being a capable adult. You should also have regular cash flow and ...') }}
                        </p>
                    </div>

                    <div class="col-lg-4 g-mb-40">
                        <i class="icon-education-141 u-line-icon-pro d-block g-font-size-55 g-line-height-1 g-color-darkblue g-mb-15"></i>
                        <h4 class="h4 g-color-gray-dark-v2 g-mb-10">{{ __('Credit property') }}</h4>
                        <p class="mb-0">{{ __('You can really multiply and compare simulations to find the right compromise in terms of interest rate, loan duration, monthly payments and personal contribution ...') }}</p>
                    </div>
                </div>
                <!-- End Row -->

                <!-- Row -->
                <div class="row g-mr-20--md">
                    <div class="col-lg-4 g-mb-40 g-mb-0--lg">
                        <i class="icon-finance-256 u-line-icon-pro d-block g-font-size-55 g-line-height-1 g-color-darkblue g-mb-15"></i>
                        <h4 class="h4 g-color-gray-dark-v2 g-mb-10">{{ __('Credit repayment') }}</h4>
                        <p class="mb-0">{{ __("However, sometimes due to a stroke (loss of spouse's work, illness, accident ...), the critical rate of the debt ratio has been exceeded and the rest of life is ultimately too low for .. .") }}</p>
                    </div>

                    <div class="col-lg-4 g-mb-40 g-mb-0--lg">
                        <i class="icon-finance-260 u-line-icon-pro d-block g-font-size-55 g-line-height-1 g-color-darkblue g-mb-15"></i>
                        <h4 class="h4 g-color-gray-dark-v2 g-mb-10">{{ __('Our interest rate') }}</h4>
                        <p class="mb-0">
                            {{ __('Our interest rates vary depending on the amount desired and the billing period. Our prices are relatively low and vary between 2% and 8.90%. Select an employee ...') }}
                        </p>
                    </div>

                    <div class="col-lg-4 g-mb-40 g-mb-0--lg">
                        <i class="icon-finance-009 u-line-icon-pro d-block g-font-size-55 g-line-height-1 g-color-darkblue g-mb-15"></i>
                        <h4 class="h4 g-color-gray-dark-v2 g-mb-10">{{ __('Credit') }}</h4>
                        <p class="mb-0">
                            {{ __("Assets affected by inalienability and not placed on the market under article 2397 of the civil code are not subject to a conventional mortgage. It's so consistent ...") }}
                        </p>
                    </div>
                </div>
                <!-- End Row -->
            </div>
        </div>
    </div>
</section>
<!-- End Our Pricing -->
