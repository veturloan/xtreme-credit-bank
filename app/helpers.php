<?php

function get_loan_types(){
    $loan_types = [];
    foreach (config('loan_types.list') as $k => $v){
        $loan_types[$k] = __($k);
    }
    return $loan_types;
}

function get_loan_durations(){
    $loan_durations = [];
    foreach (config('loan_durations.list') as $loan_duration){
        $loan_durations[$loan_duration] = $loan_duration.' '.__('months');
    }
    return $loan_durations;
}

function get_gender_list(){
    $genders = ['Man','Woman'];
    $list = [];
    foreach ($genders as $gender){
        $list[__($gender)] = __($gender);
    }
    return $list;
}
