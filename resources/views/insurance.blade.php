@extends('layouts.frontend-2')

@section('title')

    {{ __('Insurance') }}

@endsection

@section('content')

    <section class="container g-pt-100 g-mt-100 g-pb-100">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="{{ url('images/insurance/1.jpg') }}" alt="Image Description">
            </div>

            <div class="col-md-6">
                <div class="u-heading-v2-3--bottom g-brd-primary g-mb-10">
                    <h2 class="h3 text-uppercase g-font-weight-300 u-heading-v2__title">{{ __('Loan Insurance Accessio') }}</h2>
                </div>
                <p class="lead">{{ __('Do you choose Accessio revolving credit to meet your cash needs? For your peace of mind, XTREME CREDIT BANK advises you to take out insurance. Indeed, in the event of a problem, Accessio insurance can reimburse your credit on your behalf and financially protect your loved ones.') }}</p>
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="list-unstyled g-color-gray-dark-v4 g-mb-30 g-mb-0--sm">
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('All-in-one protection in the event of job loss, work stoppage, disability and death.') }}
                            </li>
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('A reduced cost (0.65% of the total amount to be reimbursed) that does not change your monthly payment') }}
                            </li>
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('An insurance cost that decreases with each reimbursement.') }}
                            </li>
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('A single point of contact to guide you') }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="g-pt-100 g-pb-100 g-bg-secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="u-heading-v2-3--bottom g-brd-primary g-mb-30">
                        <h2 class="h3 text-uppercase g-font-weight-300 u-heading-v2__title">{{ __('Loan Insurance Personal Loan') }}</h2>
                    </div>
                    <p class="lead">{{ __('Would you like to take out a Personal Loan to carry out an important project? It is equally important to protect yourself with Personal Loan Loan Loan Insurance. In the event of a problem, your credit can be repaid in your place: a real financial security for your future and that of your loved ones.') }}</p>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="list-unstyled g-color-gray-dark-v4 g-mb-30 g-mb-0--sm">
                                <li class="d-flex g-mb-10">
                                    <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                    {{ __('Coverage in the event of death, disability, illness, and loss of employment if the option is purchased.') }}
                                </li>
                                <li class="d-flex g-mb-10">
                                    <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                    {{ __('A fixed monthly contribution (from 0.16% of the borrowed capital for death, disability and total temporary incapacity to work benefits for a borrower).') }}
                                </li>
                                <li class="d-flex g-mb-10">
                                    <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                    {{ __('Your debt can be repaid at 100% in the event of death or loss of autonomy.') }}
                                </li>
                                <li class="d-flex g-mb-10">
                                    <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                    {{ __('A single point of contact to guide you') }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <img class="img-fluid" src="{{ url('images/insurance/2.jpg') }}" alt="Image Description">
                </div>
            </div>
        </div>
    </section>

    <section class="container g-pt-100 g-pb-100">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="{{ url('images/insurance/3.jpg') }}" alt="Image Description">
            </div>

            <div class="col-md-6">
                <div class="u-heading-v2-3--bottom g-brd-primary g-mb-10">
                    <h2 class="h3 text-uppercase g-font-weight-300 u-heading-v2__title">{{ __('Loan repurchase insurance') }}</h2>
                </div>
                <p class="lead">{{ __('With the redemption of credits, you group your credits into one with a reduced monthly payment. But if something happens to you, this monthly payment can become difficult to repay. With loan repurchase insurance, your credit can be taken care of and you will avoid financial difficulties for yourself and your loved ones.') }}</p>
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="list-unstyled g-color-gray-dark-v4 g-mb-30 g-mb-0--sm">
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('A customizable package with 2 choices of coverage: death/PTIA+ITT or death/PTIA/ITT+Loss of employment') }}
                            </li>
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('A contribution adapted to your coverage') }}
                            </li>
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('Coverage of the entire capital borrowed in the event of death or total disability') }}
                            </li>
                            <li class="d-flex g-mb-10">
                                <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
                                {{ __('A single point of contact to guide you') }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
