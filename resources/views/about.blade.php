@extends('layouts.frontend-2')

@section('title')

    {{ __('About Us') }}

@endsection

@section('content')

    <section class="container g-pt-100 g-mt-100 g-pb-100">
        <article class="row">
            <!-- Article Content -->
            <div class="col-lg-6 g-mb-30">
                <header class="u-heading-v6-2 g-mb-20">
                    <h6 class="text-uppercase g-font-weight-600 g-font-size-12 g-pl-90">XTREME CREDIT BANK</h6>
                    <h3 class="text-uppercase g-font-weight-600 u-heading-v6__title g-brd-primary g-color-black g-mb-15">{{ __('How we are?') }}</h3>
                </header>

                <div class="g-pl-90--sm">
                    <p class="g-mb-40">{{ __('XTREME CREDIT BANK is a consumer credit specialist all over the world, who supports you on a daily basis in each of your projects and offers you an adapted financing solution! A professional in personal finance services, XTREME CREDIT BANK is a world leader in the consumer credit market. In a constant concern to support consumers in their life projects, our expert loan advisors are attentive to your needs. They offer you adapted solutions, in branch or online, according to the monthly payments you have chosen.') }}</p>
                </div>
            </div>
            <!-- End Article Content -->

            <!-- Article Image -->
            <div class="col-lg-6 align-self-center">
                <figure class="text-center">
                    <!-- Figure Image -->
                    <img class="mx-auto" src="{{ asset('images/team/1.jpg') }}" alt="Image Description">
                    <!-- End Figure Image-->

                    <!-- Figure Info -->
                    {{--<div class="text-center g-bg-white g-pa-25">
                        <div class="g-mb-15">
                            <h4 class="h5 g-color-black g-mb-5">Laurent MICHEL</h4>
                            <em class="d-block g-font-style-normal g-font-size-11 text-uppercase g-color-primary">{{ __('President of the Bank') }}</em>
                        </div>
                    </div>--}}
                    <!-- End Figure Info-->
                </figure>
            </div>
            <!-- End Article Image -->
        </article>
    </section>

{{--    @include('includes.team')--}}

        <!-- Life at -->
        <div class="g-bg-img-hero g-bg-pos-top-center g-bg-size-cover g-bg-cover g-bg-black-opacity-0_5--after" style="background-image: url({{ url('images/vision.jpg') }});">
            <div class="container g-pos-rel g-z-index-1 g-pt-100 g-pb-50">
                <div class="row">
                    <!-- Life at - Carousel 1 -->
                    <div id="" class="col-lg-7 g-pl-300--md g-mb-50"></div>
                    <div id="" class="col-lg-5 g-pl-40--lg g-mb-50" data-infinite="true" data-fade="true" data-nav-for="#carouselCus4">
                        <div class="">
                            <h3 class="g-color-white mb-4">{{ __('Our vision') }}</h3>
                            <p class="g-color-white-opacity-0_8 g-line-height-2">
                                {{ __('Being a partner company means supporting its customers at every stage of their lives and offering them the highest quality. It means guaranteeing clear and fast answers to each request. It means having a team of real credit specialists, constantly trained. Being a responsible credit company also means accepting the strictest controls. XTREME CREDIT BANK is committed to this.') }}
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Life at - Carousel 2 -->
            </div>
        </div>
        <!-- End Life at -->

    <section class="container g-pt-100 g-pb-100">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="{{ url('images/mission.jpg') }}" alt="Image Description">
            </div>

            <div class="col-md-6">
                <div class="u-heading-v2-3--bottom g-brd-primary g-mb-10">
                    <h2 class="h3 text-uppercase g-font-weight-300 u-heading-v2__title">{{ __('Our mission') }}</h2>
                </div>
                <p class="lead">{{ __('XTREME CREDIT BANK is committed as a responsible lender by offering loans adapted to the needs of customers and their budgets, and by training and raising awareness among XTREME CREDIT BANK employees of the issues related to the prevention of over-indebtedness and the risk of financial exclusion.') }}</p>
            </div>
        </div>
    </section>

    <section class="g-pt-100 g-pb-100 g-bg-secondary">
        <div class="g-pos-rel container">
            <div class="row justify-content-between">
                <div class="col-lg-5 g-mb-30">
                    <div class="g-mt-20 mb-5">
                        <h3 class="h6 text-uppercase g-font-weight-600 g-letter-spacing-2 mb-3">1998 - 2019</h3>
                        <h2 class="mb-4">{{ __('Our History') }}</h2>
                        <p class="g-line-height-2">{{ __('At your side for more than 20 years. At the end of the founding of the XTREME CREDIT BANK-GROUP, XTREME CREDIT BANK was created in 2008 to meet the equipment needs of households.  The credit company was globalized in 2012 and then privatized in 2013. In 2015, it was attached to the Crédit Mutuel group through a partnership, before being officially acquired in 2017.') }}</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 g-pos-abs--lg g-top-0--lg g-right-0--lg g-mb-30">
                <img class="w-100 g-width-auto--lg" src="{{ url('images/history.jpg') }}" alt="Image Description">
            </div>
        </div>
    </section>

@endsection
